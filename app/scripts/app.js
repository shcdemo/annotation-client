/*
Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/


  'use strict';

jQuery(function () {
  window.addEventListener('startAnnotation', function () {
    console.log("annotation started")
  });

  // Grab a reference to our auto-binding template
  // and give it some initial binding values
  // Learn more about auto-binding templates at http://goo.gl/Dx1u2g
  var app = document.querySelector('#app');

  // Sets app default base URL
  app.baseUrl = '/';
  if (window.location.port === '') {  // if production
    // Uncomment app.baseURL below and
    // set app.baseURL to '/your-pathname/' if running from folder in production
    // app.baseUrl = '/polymer-starter-kit/';
  }

  app.highState = true

  // Listen for template bound event to know when bindings
  // have resolved and content has been stamped to the page
  app.addEventListener('dom-change', function(e) {
    console.log('Our app is ready to rock!');
  });

  WebComponents.waitFor(() => {
    // imports are loaded and elements have been registered

    var doc = document.getElementById('A00456')

    var highToggle = document.getElementById('hhhh');
    if (highToggle) {
      highToggle.addEventListener('click', function () {
        app.highState = !app.highState
        doc.highlight = !doc.highlight;
      })
    }

    var spellToggle = document.getElementById('pppp')
    if (spellToggle) {
        var showStandardSpellings = false;
        spellToggle.addEventListener('click', function () {
            showStandardSpellings = !showStandardSpellings;
            var nodeList = document.querySelectorAll('tei-w[orig]');
            for (var l = 0; l < nodeList.length; l++) {
                var w = nodeList[l];
                if (showStandardSpellings) {
                    w.innerHTML = w.getAttribute('reg');
                }
                else {
                    w.innerHTML = w.getAttribute('orig');
                }
        }
      })
    }
  });

  jQuery(".tooltip").remove();
  jQuery('body').tooltip({
      selector: 'tei-w',
      delay: 500,
      html: true,
      container: 'body',
      title: function (el) {
          var lemma = el.getAttribute('lemma');
          var reg = el.getAttribute('reg');
          var pos = el.getAttribute('pos');
          return '<div>' +
              '<span>id: </span><span>' + el.id + '</span><br/>' +
              '<span>lemma: </span><span>' + (lemma || 'N/A') + '</span><br/>' +
              '<span>reg: </span><span>' + (reg || el.innerHTML) + '</span><br/>' +
              '<span style="text-align: left">pos: </span><span>' + (pos || 'N/A') + '</span></div>';
      }
  });

  app.showAnnotationEditor = function () {
    app.$.annotationEditor.show()
  }
});

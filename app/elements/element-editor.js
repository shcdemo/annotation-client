import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { Annotation } from './annotation.js';

class ElementEditor extends PolymerElement {
  static get template() {
    return html `
    <style>
      h1, h2, h3, h4, h5, h6 {
        font-family: "IM Fell English", serif;
        line-height: 1.2;
      }

      .btn-block {
        display: block;
        width: 100%;
        height: 24px;
      }

      .btn {
        border: none;
        border-radius: 2px;
        margin: 5px 1px;
        text-transform: uppercase;
        letter-spacing: 0;
        will-change: box-shadow, transform;
        transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1), color 0.2s cubic-bezier(0.4, 0, 0.2, 1);
        outline: 0;
        cursor: pointer;
        text-decoration: none;
        background: 0 0;
      }

      .btn:hover {
        color: #333;
        background-color: #e6e6e6;
        border-color: #adadad;
      }

      .btn:disabled {
        opacity: 0.5;
        cursor: not-allowed;
      }

      .btn:disabled:hover {
        background-color: transparent;
        color: inherit;
      }

      :host {
        display: block;
      }
      .actions {
        margin-top: 0.75rem;
        -webkit-display: -webkit-box;
        -webkit-display: -webkit-flex;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        align-items: center;
        justify-content: flex-end;
      }

      h4 {
        font-size: 16px;
        font-weight: bold;
        margin: 1.5rem 0 0;
      }
      .editor-row {
        width: auto;
      }
      .editor-row.no-title {
        margin-top: 0.75rem;
        display: inline-flex;
      }
      .editor-row.space-before {
        margin-top: 1.5rem;
      }
      .editor-row .old {
        width: 49%;
        display: inline;
        font-size: 16px;
      }
      .delete_button {
        color: red;
      }
      .delete_button:hover {
        background: darkred;
        color: white;
      }
      .delete_button[disabled] {
        color: darkred;
      }
      .editor-row .btn-block {
        margin-top: 10px;
      }

      @keyframes highlight-fade {
        0% {
          background: lightyellow;
        }
        100% {
          background: white;
        }
      }

      .old{
        font-family: monospace;
      }

      .highlight .old {
        animation: highlight-fade 1s ease-out;
      }

      .material-icons {
        vertical-align: middle;
      }

      input[type="checkbox"] {
        width: 18px;
        height: 18px;
        vertical-align: middle;
        margin-right: 0.5rem;
      }

      textarea:focus, input[type="text"]:focus {
        outline: 0;
        background-size: 100% 2px,100% 1px;
        transition-duration: .3s;
      }

      textarea, input[type="text"] {
        display: inline-block;
        position: relative;
        padding: 2px;
        margin-top: 0.3rem;
        overflow: hidden;
        resize: none;
        border: 0;
        background-image: linear-gradient(#4f4f4f, #4f4f4f), linear-gradient(#D2D2D2, #D2D2D2);
        background-size: 0 2px,100% 1px;
        background-repeat: no-repeat;
        background-position: center bottom, center calc(100% - 1px);
        background-color: rgba(0, 0, 0, 0);
        transition: background 0s ease-out;
        box-shadow: none;
        border-radius: 0;
        font-family: inherit;
        font-size: 16px;
      }

      textarea {
        width: 100%;
        float: none;
      }

      input[type="text"] {
        float: right;
        margin-bottom: 1rem;
        margin-left: 2rem;
      }

      details > summary {
        font-size: 12px;
        display: flex;
        width: 100%;
        background-color: #E8E8E8;
        padding: 8px 10px 2px 10px;
        text-transform: uppercase;
        cursor: pointer;
        list-style: none;
        justify-content: space-between;
      }

      summary::-webkit-details-marker {
        display: none;
      }

      summary::after {
        font-family: "Material Icons";
        font-size: 16px;
        content: "\\e5cf";
      }

      details[open] > summary::after {
        font-family: "Material Icons";
        font-size: 16px;
        content: "\\e5ce";
      }

      details > summary:hover {
        color: #333;
        background-color: #c4bbbb;
        border-color: #adadad;
      }
    </style>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    <div class="editor-row">
      <h4>Text</h4>
      <span class="old">[[text.current]]</span>
      <input class="input" type="text" id="textInput" value="{{text.value}}" name="text" placeholder="Correction"></input>
    </div>

    <div class="editor-row">
      <h4>Standard</h4>
      <span class="old">[[reg.current]]</span>
      <input type="text" id="regInput" value="{{reg.value}}" name="reg" placeholder="Regularise"></input>
    </div>

    <details id="collapse_morphology">
      <summary>Morphology (optional)</summary>

      <div class="editor-row">
        <h4>Lemma</h4>
        <span class="old">[[lemma.current]]</span>
        <input type="text" id="lemmaInput" value="{{lemma.value}}" name="lemma" placeholder="Change Lemma"></input>
      </div>

      <div class="editor-row">
        <h4>Part of Speech (pos)</h4>
        <span class="old">[[pos.current]]</span>
        <input type="text" id="posInput" value="{{pos.value}}" placeholder="Set part of speech" name="pos"></input>
      </div>

    </details>

    <div class="editor-row no-title">
      <input type="checkbox" id="defectInput" name="defect" checked="{{defect.value}}" value="true"></input>
      <span>mark as defective</span>
    </div>

    <div class="editor-row space-before">
      <textarea id="noteInput" value="{{note.value}}" name="note" placeholder="Add a note to your change set" rows="1"></textarea>
    </div>

    <div class="actions">
      <button class="btn delete_button" on-click="deleteWord" disabled="[[deleteDisabled]]">Delete Word</button>
      <button class="btn" on-click="cancel">Cancel</button>
      <button class="btn" on-click="handleApplyButtonTap" disabled="[[submitDisabled]]">Save</button>
    </div>
`;
  }

  static get behaviors() {
    return Annotation.ActionTypes;
  }

  static get properties() {
    return {
      text: {
        type: Object,
        value: function () { return {} }
      },
      lemma: {
        type: Object,
        value: function () { return {} }
      },
      reg: {
        type: Object,
        value: function () { return {} }
      },
      pos: {
        type: Object,
        value: function () { return {} }
      },
      defect: {
        type: Object,
        value: function () { return {} }
      },
      note: {
        type: Object,
        value: function () { return {} }
      },
      allowedActions: {
        type: Array,
        value: function () { return [] }
      },
      fields: {
        type: Array,
        value: function () { return ['text', 'lemma', 'reg', 'pos'] }
      },
      submitDisabled: {
        type: Boolean,
        value: true
      },
      deleteDisabled: {
        type: Boolean,
        value: false
      }
    }
  }

  connectedCallback() {
    this.updateStyles();
  }

  ready() {
    super.ready();
    const textInput = this.shadowRoot.getElementById("textInput");
    textInput.addEventListener('input', event => this._textChanged(event));

    var inputs = ['regInput', 'lemmaInput', 'posInput', 'defectInput', 'noteInput'];
    for (var index = 0; index < inputs.length; index++) {
      const input = this.shadowRoot.getElementById(inputs[index]);
      input.addEventListener('input', event => this._handleValueChange(event));
    }
    this.updateStyles();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    const textInput = this.shadowRoot.getElementById("textInput");
    textInput.removeEventListener('input', event => this._textChanged(event));

    var inputs = ['regInput', 'lemmaInput', 'posInput', 'defectInput', 'noteInput'];
    for (var index = 0; index < inputs.length; index++) {
      const input = this.shadowRoot.getElementById(inputs[index]);
      input.removeEventListener('input', event => this._handleValueChange(event));
    }
  }

  setAllowedActions(allowedActions) {
    this.allowedActions = allowedActions;
  }

  setTargets (targets, annotations) {
    if (this.text.current === '') {
      this.set(['text.current'], targets.reduce(Annotation.textReducer, ''));
    }

    var mainTarget = targets[0]
    var reg;
    if (this.reg.current === '') {
      reg = mainTarget.getAttribute('reg');
      if (!reg || reg.length === 0) {
        reg = mainTarget.innerHTML;
      }
      // Eliminate inner tagging such as <mark>
      var elem = new DOMParser().parseFromString(reg, 'text/html');
      reg = elem.body.textContent || '';

      this.set(['reg.current'], reg);
    }

    this.targets = targets;
    this.deleteDisabled = this.targets.length < 1;

    if (this.lemma.current === '') {
      this.set(['lemma.current'], targets[0].getAttribute('lemma'));
    }
    if (this.pos.current === '') {
      this.set(['pos.current'], targets[0].getAttribute('pos'));
    }
  }

  cancel() {
    this._reset();
    this.dispatchEvent(new CustomEvent('cancelled', {bubbles: true, composed: true}));
  }

  handleApplyButtonTap (event) {
    event.stopPropagation();
    this.save();
  }

  // determine type of action from user input
  save() {
    if (!this.targets) {
      this._reset();
      this.dispatchEvent(new CustomEvent('closed', {bubbles: true, composed: true}));
    }

    var action = {
      ticket: Annotation.getNewSessionKey(),
      targets: this.targets
    }

    if (this.text.dirty && !this.$.textInput.invalid) {
      console.log('text');
      this.dispatchEvent(new CustomEvent('saved',
        {
          bubbles: true,
          composed: true,
          detail: {
            ticket: action.ticket,
            targets: action.targets,
            body: this.text.value,
            action: Annotation.ActionTypes.UPDATE,
            id: this.text.annotationId,
            originalValue: this.text.current
          }
        }
      ));
    }

    if (this.reg.dirty && !this.$.regInput.invalid) {
      console.log('regularize');
      this.dispatchEvent(new CustomEvent('saved',
        {
          bubbles: true,
          composed: true,
          detail: {
            ticket: action.ticket,
            targets: action.targets,
            body: this.reg.value,
            attributeName: 'reg',
            action: Annotation.ActionTypes.UPDATE_ATTRIBUTE,
            id: this.reg.annotationId
          }
        }
      ));
    }

    if (this.lemma.dirty && !this.$.lemmaInput.invalid) {
      console.log('lemma');
      this.dispatchEvent(new CustomEvent('saved', 
      {
        bubbles: true,
        composed: true,
        detail: {
          ticket: action.ticket,
          targets: action.targets,
          body: this.lemma.value,
          attributeName: 'lemma',
          action: Annotation.ActionTypes.UPDATE_ATTRIBUTE,
          id: this.lemma.annotationId
        }
      }));
    }

    if (this.pos.dirty && !this.$.posInput.invalid) {
      console.log('pos');
      this.dispatchEvent(new CustomEvent('saved', 
      {
        bubbles: true,
        composed: true,
        detail: {
          ticket: action.ticket,
          targets: action.targets,
          body: this.pos.value,
          attributeName: 'pos',
          action: Annotation.ActionTypes.UPDATE_ATTRIBUTE,
          id: this.pos.annotationId
        }
      }));
    }

    // oldDefect can be undefined, false or true
    // compare with '!=' to match 'undefined == false'
    if (this.defect.dirty) {
      console.log('defect');
      this.dispatchEvent(new CustomEvent('saved', 
      {
        bubbles: true,
        composed: true,
        detail: {
          ticket: action.ticket,
          targets: action.targets,
          body: this.defect.value,
          attributeName: 'defect',
          action: Annotation.ActionTypes.UPDATE_ATTRIBUTE,
          id: this.defect.annotationId
        }
      }));
    }

    if (this.note.dirty) {
      console.log('add note');
      this.dispatchEvent(new CustomEvent('saved', 
      {
        bubbles: true,
        composed: true,
        detail: {
          ticket: action.ticket,
          targets: action.targets,
          body: this.note.value,
          action: Annotation.ActionTypes.NOTE,
          id: this.note.annotationId
        }
      }));
    }

    this._reset();
    this.dispatchEvent(new CustomEvent('closed',
    {
      bubbles: true,
      composed: true,
      detail: {
        ticket: action.ticket,
        targets: action.targets
      }
    }));

  }

  _validateText(text) {
    return text.trim().length > 0
  }

  // determine type of action from user input
  _textChanged(event) {
    this.$.textInput.invalid = !this._validateText(this.$.textInput.value);
    this.text.dirty = true;
    this.text.value = this.$.textInput.value;
    this._checkSubmitDisabled();
  }

  deleteWord(e) {
    var action = {
      ticket: Annotation.getNewSessionKey(),
      targets: this.targets.slice()
    };

    console.log('delete');
    this.dispatchEvent(new CustomEvent('saved',
    {
      bubbles: true,
      composed: true,
      detail: {
        ticket: action.ticket,
        targets: action.targets,
        body: '',
        action: Annotation.ActionTypes.DELETE,
        originalValue: this.text.current
      }
    }));

    if (this.note.dirty) {
      console.log('add note');
      this.dispatchEvent(new CustomEvent('saved',
      {
        bubbles: true,
        composed: true,
        detail: {
          ticket: action.ticket,
          targets: action.targets,
          body: this.note.value,
          action: Annotation.ActionTypes.NOTE,
          id: this.note.annotationId,
        }
      }));
    }

    this._reset();
    this.dispatchEvent(new CustomEvent('editor.closed', {bubbles: true, composed: true}));
  }

  submit(e) {
    if (e.which === 13 || e.keyCode === 13) {
      this.save();
    }
  }

  getNewCleanValue(value) {
    return {
      value: value,
      current: '',
      dirty: false
    }
  }

  _reset() {
    this.$.textInput.invalid = false;
    this.text = this.getNewCleanValue('');
    this.reg = this.getNewCleanValue('');
    this.pos = this.getNewCleanValue('');
    this.lemma = this.getNewCleanValue('');
    this.note = this.getNewCleanValue('');
    this.defect = this.getNewCleanValue(false);
    this.submitDisabled = true;
    this.deleteDisabled = !this.submitDisabled;
  }

  // observers
  _handleValueChange (event) {
    this[event.target.name].dirty = true;
    this[event.target.name].value = event.target.value;
    this._checkSubmitDisabled();
  }

  _checkSubmitDisabled () {
    this.submitDisabled = !(
      this.text.action === '' ||
      this.text.dirty ||
      this.reg.dirty ||
      this.pos.dirty ||
      this.lemma.dirty ||
      this.defect.dirty
    ) ||
      this.$.textInput.invalid ||
      this.$.regInput.invalid ||
      this.$.posInput.invalid ||
      this.$.lemmaInput.invalid;
    this.deleteDisabled = !this.submitDisabled;
  }

}

customElements.define('element-editor', ElementEditor);
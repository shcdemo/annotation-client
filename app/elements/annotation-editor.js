import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { Annotation } from './annotation.js';

import './element-editor.js';
import './join-editor.js';
import './split-editor.js';
import './comment-editor.js';

class AnnotationEditor extends PolymerElement {
  static get template() {
    return html`
    <style>
      dile-tabs {
        background: white;
        --dile-tab-selected-line-color: var(--paper-blue-900);
        --dile-tab-selected-text-color: black;
        --dile-tab-selected-background-color: transparent;
        --dile-tab-selected-line-height: 2px;
        --dile-tab-text-transform: none;
        --dile-tab-font-size: 14px;
        --dile-tab-font-weight: 500;
      }

      dile-tab {
        text-align: center;
        width: 25%;
      }

      dile-pages section {
        margin: 0 2rem 2rem;
      }
    </style>
    <dile-tabs id="tabs" selected="{{selected}}" attrForSelected="name" selectorId="whicheditor">
      <dile-tab name="edit" inert="[[editDisabled]]">edit</dile-tab>
      <dile-tab name="split" inert="[[splitDisabled]]">split</dile-tab>
      <dile-tab name="join" inert="[[joinDisabled]]">join</dile-tab>
      <dile-tab name="comment">comment</dile-tab>
    </dile-tabs>
    <dile-pages selected="[[selected]]" attrforselected="name" selectorId="whicheditor">
      <section name="edit"><element-editor id="elementEditor"></element-editor></section>
      <section name="split"><split-editor id="splitEditor"></split-editor></section>
      <section name="join"><join-editor id="joinEditor"></join-editor></section>
      <section name="comment"><comment-editor id="commentEditor"></comment-editor></section>
    </dile-pages>
`;
  }

  static get properties() {
    return {
      editDisabled: {
        type: Boolean,
        value: false
      },
      selectionInfo: {
        type: String,
        value: ''
      },
      elementType: {
        type: String,
        value: 'word'
      },
      selected: {
        value: 0
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();
    this.hidden = true;

    var editors = ['elementEditor', 'commentEditor', 'joinEditor', 'splitEditor'];
    for (var index = 0; index < editors.length; index++) {
      const editor = this.shadowRoot.getElementById(editors[index]);
      editor.addEventListener('close', event => this.close(event));
    }
    var tabs = this.shadowRoot.getElementById('tabs');
    tabs.addEventListener('dile-selected-changed', event => this.selectedChanged(event));
  }
  selectedChanged(e) {
    if (e.detail.selected === "edit" && this.editDisabled) {
      return false;
    }
  }

  created() {
    this.actions = [];
  }

  setAllowedActions(allowedActions) {
    this.allowedActions = allowedActions;
    this.$.elementEditor.allowedActions = allowedActions;
  }

  setSelection(targets, annotations) {
    this._reset();

    this.$.commentEditor.setTargets(targets);

    // enable editing
    this.editDisabled = (targets.length > 1);
    this.splitDisabled = (targets.length > 1);
    this.joinDisabled = (targets.length === 1);

    function setSelectedTabFromNumberOfTargets(numberOfTargets) {
      if (numberOfTargets === 1) return "edit";
      if (numberOfTargets > 1) return "join";
      return "comment";
    }
    this.selected = setSelectedTabFromNumberOfTargets(targets.length);

    annotations.forEach(this.setOriginalValue, this);
    this.$.elementEditor.setTargets(targets);
    this.$.joinEditor.setTargets(targets);
    this.$.splitEditor.setTargets(targets);
  }

  loadAnnotations(annotations) {
    console.log('loadAnnotations', annotations);
    annotations.forEach(this.loadAnnotation, this);
    this.setEditorTab(annotations[0]);
  }

  open() {
    this.hidden = false;
  }

  close() {
    this.hidden = true;
    this._reset();
  }

  loadAnnotation(annotation) {
    console.log('loadAnnotation', annotation);
    if (!annotation) { return }
    switch (annotation.body.getAttribute('subtype')) {
      case Annotation.ActionTypes.JOIN:
        this.$.joinEditor.set('text.current', annotation.body.originalValue);
        this.$.joinEditor.set('text.value', annotation.body.display);
        this.$.joinEditor.set('text.annotationId', annotation.id);
        break;
      case Annotation.ActionTypes.SPLIT:
        this.$.splitEditor.set('text.current', annotation.body.originalValue);
        this.$.splitEditor.set('text.value', annotation.body.display);
        this.$.splitEditor.set('text.annotationId', annotation.id);
        break;
      case Annotation.ActionTypes.UPDATE:
        this.$.elementEditor.set('text.current', annotation.body.originalValue);
        this.$.elementEditor.set('text.value', annotation.body.display);
        this.$.elementEditor.set('text.annotationId', annotation.id);
        break;
      case Annotation.ActionTypes.UPDATE_ATTRIBUTE:
        var attribute = annotation.body.querySelector('attribute').getAttribute('name');
        this.$.elementEditor.set([attribute, 'current'], annotation.body.originalValue);
        this.$.elementEditor.set([attribute, 'value'], annotation.body.display);
        this.$.elementEditor.set([attribute, 'annotationId'], annotation.id);
        this.$.joinEditor.set([attribute, 'current'], annotation.body.originalValue);
        this.$.joinEditor.set([attribute, 'value'], annotation.body.display);
        this.$.joinEditor.set([attribute, 'annotationId'], annotation.id);
        // Split editor will have two reg/lemma/pos attributes and a generated
        // ID ending in -s-3 for the second instance.
        var targetId  = annotation.querySelector('target-selector').getAttribute('value');
        if ((/-s-3$/.test(targetId))) {
          attribute += '2';
        }
        this.$.splitEditor.set([attribute, 'current'], annotation.body.originalValue);
        this.$.splitEditor.set([attribute, 'value'], annotation.body.display);
        this.$.splitEditor.set([attribute, 'annotationId'], annotation.id);
        break;
      case Annotation.ActionTypes.NOTE:
        this.$.elementEditor.set('note.value', annotation.body.display);
        this.$.elementEditor.set('note.annotationId', annotation.id);
        this.$.joinEditor.set('note.value', annotation.body.display);
        this.$.joinEditor.set('note.annotationId', annotation.id);
        this.$.splitEditor.set('note.value', annotation.body.display);
        this.$.splitEditor.set('note.annotationId', annotation.id);
        break;
      case Annotation.ActionTypes.COMMENT:
        this.$.commentEditor.set('comment.value', annotation.body.display);
        this.$.commentEditor.set('comment.annotationId', annotation.id);
        this.$.commentEditor.set('private.value', (annotation.visibility === 'private'));
        break;
      default:
        console.log('cannot apply', annotation);
    }
  }

  // Sending this the first annotation in the ticket should get us the overall
  // type; if we got an attribute update or note we wouldn't know which tab it
  // came from.
  setEditorTab(annotation) {
    if (!annotation) { return }
    switch (annotation.body.getAttribute('subtype')) {
      case Annotation.ActionTypes.JOIN:
        this.selected = 'join';
        break;
      case Annotation.ActionTypes.SPLIT:
        this.selected = 'split';
        break;
      case Annotation.ActionTypes.COMMENT:
        this.selected = 'comment';
        break;
      default:
        this.selected = 'edit';
    }

  }
  setOriginalValue(annotation) {
    console.log('setOriginalValue', annotation);
    if (!annotation) { return }
    switch (annotation.body.getAttribute('subtype')) {
      case Annotation.ActionTypes.JOIN:    // fallthrough
      case Annotation.ActionTypes.SPLIT:   // fallthrough
      case Annotation.ActionTypes.UPDATE:
        this.$.elementEditor.set('text.current', annotation.body.originalValue);
        this.$.joinEditor.set('text.current', annotation.body.originalValue);
        this.$.splitEditor.set('text.current', annotation.body.originalValue);
        break;
      case Annotation.ActionTypes.UPDATE_ATTRIBUTE:
        var attribute = annotation.body.querySelector('attribute').getAttribute('name');
        this.$.elementEditor.set([attribute, 'current'], annotation.body.originalValue);
        this.$.joinEditor.set([attribute, 'current'], annotation.body.originalValue);
        this.$.splitEditor.set([attribute, 'current'], annotation.body.originalValue);
        break;
      default:
        console.log('cannot apply', annotation);
    }
  }

  _reset() {
    this.$.elementEditor._reset();
    this.$.commentEditor._reset();
    this.$.joinEditor._reset();
    this.$.splitEditor._reset();
  }
}

customElements.define('annotation-editor', AnnotationEditor);


import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import * as Gestures from '@polymer/polymer/lib/utils/gestures.js';
import { Annotation } from './annotation.js';

class AnnotatableDocument extends PolymerElement {

  static get template() {
    return html`
  <style>
  h1, h2, h3, h4, h5, h6 {
    font-family: "IM Fell English", serif;
    line-height: 1.2;
  }

  :host {
    display: block;
    position: relative;
    margin: 1rem;
  }

  /* annotated element states */
  ::slotted .selected {
    background: lightblue;
  }

  /* atomic element styles */
  ::slotted .atomic {
    position: relative;
  }

  /* only block text selection for highlighted documents*/
  ::slotted .atomic.selectable {
    -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none;   /* Chrome/Safari/Opera */
    -khtml-user-select: none;    /* Konqueror */
    -moz-user-select: none;      /* Firefox */
    -ms-user-select: none;       /* Internet Explorer/Edge */
    user-select: none;           /* Non-prefixed version, currently
                                     not supported by any browser */
  }
  </style>
  <div id="document">
    <slot name="document"></slot>
  </div>
`;
  }

  /**
   * return highest selectable element
   */
  findSelectableInPath(path) {
    var selectables = path.filter(function (nextElementInPath) {
      if (nextElementInPath.classList) {
        return nextElementInPath.classList.contains("selectable");
      }
    })
    return selectables.pop();
  }

  getSelection(startEvent, endEvent) {
    console.log('getSelection', startEvent, endEvent);
    var start = startEvent.element;
    var end = endEvent.element;

    // was the selection done in reverse order
    // NOTE checking for bottom to top caused too many false positives
    // therefore only right to left supported
    // TODO check if line boundary is crossed for bottom to top
    // swap start and end of selection
    if (startEvent.x > endEvent.x /*|| startEvent.y < endEvent.y */) {
      start = endEvent.element;
      end = startEvent.element;
      startEvent = endEvent;
    }

    // maybe first element is a tei-c try skipping that
    if (!start.classList.contains('selectable')) {
      console.debug('skipping non-selectable start element');
      var next = start.nextElementSibling;
      start = next;
    }

    // all attempts failed = give up
    if (!start.classList.contains('selectable')) {
      console.debug('start.element', start, 'not selectable');
      return null;
    }

    return Annotation.Selection.getElementsInRange([start, end]);
  }

  setSelected(state) {
    return function (element) {
      if (element.orig) {
        element.orig.selected = state;
      }
      else {
        element.selected = state;
      }
      if (typeof element._setStyle === "function") {
        element._setStyle();
      }
      return state;
    }
  }

  static get properties() {
    return {
      panel: String,
      selection: {
        type: Array,
        value: function () { return [] }
      },
      version: {
        type: String
      },
      highlight: {
        observer: '_handleHighlightChange',
        type: Boolean,
        value: true
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();
    this._panel = document.getElementById(this.panel);
    // if we didn't find it in the light DOM, try the shadow DOM
    if (this._panel === undefined || this._panel === null) {
      let host = this.getRootNode().host.shadowRoot;
      this._panel = host.getElementById(this.panel);
    }
    if (this._panel && this.highlight) {
      this._addListeners();
    }
  }
  disconnectedCallback() {
    super.disconnectedCallback();
    this._removeListeners();
  }

  selectSingle(event) {
    this.cancelSelection();
    var path = event.composedPath();
    var selectedElement = this.findSelectableInPath(path);
    // user clicked on tei-c, tei-l or something else
    if (!selectedElement) {
      console.debug('could not process selection', path);
      return this._handleSelection();
    }
    return this._handleSelection([selectedElement]);
  }

  select(event) {
   // record start element
    if (event.detail.state === 'start') {
      this.cancelSelection();
      this._selectionStart = {
        element: event.detail.sourceEvent.target,
        path: event.composedPath(),
        x: event.detail.x,
        y: event.detail.y
      };
    }

    // record end element
    if (event.detail.state === 'end') {
      var selectionEnd = {
        element: event.detail.sourceEvent.target,
        path: event.composedPath(),
        x: event.detail.x,
        y: event.detail.y
      };
      var selectedElements = this.getSelection(this._selectionStart, selectionEnd);
      this._handleSelection(selectedElements);
    }
  }

  _handleSelection(selection) {
    if (!selection) {
      this._panel.reset();
      this.cancelSelection();
      return;
    }

    this.selection = selection;
    this.selection.forEach(this.setSelected(true));
    this._panel.setSelection(this, selection);

    // signal to other display components
    // that words were selected in the document
    this.dispatchEvent(new CustomEvent('wordsSelected',
    {
      bubbles: true,
      composed: true,
      detail: {
        selection: selection
      }
    }));
  }

  cancelSelection() {
    this.selection.forEach(this.setSelected(false));
    this.selection = [];
  }

  _addListeners() {
    this._panel.addEventListener('cancel.selection', event => this.cancelSelection(event));
    let node = this.shadowRoot ? this.shadowRoot : this;
    Gestures.addListener(node, 'tap', event => this.selectSingle(event));
    Gestures.addListener(node, 'track', event => this.select(event));
  }

  _removeListeners() {
    this._panel.removeEventListener('cancel.selection', event => this.cancelSelection(event));
    let node = this.shadowRoot ? this.shadowRoot : this;
    Gestures.removeListener(node, 'tap', event => this.selectSingle(event));
    Gestures.removeListener(node, 'track', event => this.select(event));
  }

  _handleHighlightChange(newValue) {
    if (!this._panel) { return } // only for documents with a panel (attached)

    var newHighlightState = !!newValue; // force boolean value

    // toggle higlighting of atomic child elements
    var atoms = this.querySelectorAll('.atomic')
    for (var atom of atoms) {
      this.toggleClass('selectable', newHighlightState, atom);
    }

    if (newHighlightState) {
      return this._addListeners(); // enable selection
    }

    this._panel.reset();
    this.cancelSelection();
    this._removeListeners();
  }

  ready() {
    super.ready();
  }

  /**
   * Toggles a CSS class on or off.
   * Borrowed from LegacyElementMixin because importing that doesn't work and
   * this method is all we need from it.
   *
   * @param {string} name CSS class name
   * @param {boolean=} bool Boolean to force the class on or off.
   *    When unspecified, the state of the class will be reversed.
   * @param {Element=} node Node to target.  Defaults to `this`.
   * @return {void}
   * @override
   */
  toggleClass(name, bool, node) {
    node = /** @type {Element} */ (node || this);
    if (arguments.length == 1) {
      bool = !node.classList.contains(name);
    }
    if (bool) {
      node.classList.add(name);
    } else {
      node.classList.remove(name);
    }
  }
}

customElements.define('annotatable-document', AnnotatableDocument);

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

import './annotation-editor.js';
import './selection-info.js';
import './annotation-list.js';

class AnnotationPanel extends PolymerElement {

  static get template() {
    return html`
    <style>
      h1, h2, h3, h4, h5, h6 {
        font-family: "IM Fell English", serif;
        line-height: 1.2;
      }
    </style>
    <selection-info id="info"></selection-info>
    <annotation-editor id="editor"></annotation-editor>
    <annotation-list server="[[server]]" user="[[user]]" id="list"></annotation-list>
    <slot name="annotations" id="contents" hidden=""></slot>
`;
  }

  static get properties() {
    return {
      server: String,
      user: {
        type: Object,
        value: function () { return {} }
      },
      annotationPromises: {
        type: Number,
        value: 0
      }
    }
  }

  ready() {
    super.ready();
    this.set('user.name', sessionStorage.getItem('user') || 'guest');
    this.set('user.role', sessionStorage.getItem('role') || 'reader');
    if (this.user.role !== 'reader') {
      this._editListener = this.$.list.addEventListener('edit', event => this.openEditor(event));
    }

    const editor = this.shadowRoot.getElementById("editor");
    editor.addEventListener('closed', event => this.backToList(event));
    editor.addEventListener('cancelled', event => this.backToList(event));
    editor.addEventListener('saved', event => this.handleAnnotationSave(event));


    // Polymer 3 completely breaks the contract upon which the Polymer 1 app depended in terms
    // of when attached/connected/ready events fire and the ordering of when children elements
    // are loaded in relation to parents.  And <content> is broken and replaced by <slot>.  So
    // we've had to move the slot to the parent (because otherwise it's invisible) and ended up
    // with a mad scramble through the shadow DOM from the parent and a bunch of imperative code
    // to set up initial state.  In the code below, that state consists of the annotations that
    // exist on the page when it is first loaded (as opposed to those the user may add later).

    const slot = this.shadowRoot.getElementById("contents");
    slot.addEventListener('slotchange', e => {
      const slotDiv = slot.assignedNodes()[0];  // should only be one
      var annotations =  slotDiv.querySelectorAll('annotation-item');
      for (var i = 0; i < annotations.length; i++) {
        var a = annotations[i];
        a.apply();
        this.$.list.annotations.push(a);
      }
      this.$.list.close();
    });
  }

  toAtomicList(element) {
    if ('orig' in element) { return element.orig }
    return element
  }
  getTickets(atomicNode) {
    return atomicNode.tickets || []
  }
  flatten(result, tickets) {
    return result.concat(tickets)
  }

  setSelection(annotatableDocument, selection) {
    this.currentDocument = annotatableDocument;
    var tickets = this._getTicketsForSelection(selection);

    // do nothing when a reader clicks on something without a ticket
    if (tickets.length === 0 && this.user.role === 'reader') {
      console.log('tickets found', tickets);
      return this.dispatchEvent(new CustomEvent('cancel.selection', { bubbles: true, composed: true }));
    }
    var acceptedAnnotations = this.$.list.annotations.filter(function (annotation) {
      return (
        tickets.indexOf(annotation.ticket) >= 0 &&
        annotation.status === 'accepted' &&
        'originalValue' in annotation.body
      );
    });
    this.selection = selection;
    this.$.info.setSelection(selection);
    this.$.editor.setSelection(selection, acceptedAnnotations);

    // open editor immediately when no ticket is associated
    if (tickets.length === 0) {
      this.$.list.close();
      this.$.editor.open();
      return;
    }

    this.$.list.filterByTickets(tickets);
    this.$.editor.close();
    this.$.list.open();
    // Extend the display selection to all the words in the first ticket so that
    // on change we'll update the display of all of them.  Display selection is
    // not related to the selected ticket (other than figuring which words are
    // affected) nor to the selected editor tab.
    tickets[0]._affected.forEach(annotatableDocument.setSelected(true));
    annotatableDocument.selection = tickets[0]._affected;
  }

  openEditor(e) {
    console.log('openEditor', e);
    if (e.detail && e.detail.data && 'annotations' in e.detail.data) {
      this.$.editor.loadAnnotations(e.detail.data.annotations);
    }
    this.$.list.close();
    this.$.editor.open();
  }

  handleAnnotationSave(event) {
    var myid = this.currentDocument.getAttribute("id");
    var mysource = this.currentDocument.getAttribute("source");
    var myversion = this.currentDocument.getAttribute("version");

    var annotationData = Object.assign({}, event.detail, {
      document: {
        id: myid,
        source: mysource,
        version: myversion
      }
    });
    // edit or reject and add with same ticket number
    // TODO check ticket ID
    if (annotationData.id) {
      return this.$.list.updateAnnotation(annotationData);
    }

    var awaitAddAnnotation = function aaa() {
      this.annotationPromises -= 1;
      if (this.annotationPromises > 0) { return }
      this.backToList();
    }.bind(this);

    // add new annotation
    this.annotationPromises += 1;
    var addPromise = this.$.list.addAnnotation(annotationData);
    addPromise
      .then(awaitAddAnnotation)
      .catch(awaitAddAnnotation);
  }

  reset() {
    this.$.list.close();
    this.$.editor.close();
    this.$.list.resetFilters();
    this.$.info.setSelection();
    this.$.list.removeEventListener('edit', this._editListener);
    this.dispatchEvent(new CustomEvent('cancel.selection', { bubbles: true, composed: true }));
  }

  backToList() {
    this.$.editor.close();
    // wait for new annotations
    if (this.annotationPromises > 0) { return }
    // update list filters
    var tickets = this._getTicketsForSelection(this.selection);
    // check list length
    if (this.$.list.tickets.length === 0 || tickets.length === 0) {
      return this.reset();
    }
    this.$.list.filterByTickets(tickets);
    this.$.list.open();
  }

  _getTicketsForSelection(selection) {
    if (!selection) { return [] }
    return selection.map(this.toAtomicList)
      .map(this.getTickets)
      .reduce(this.flatten, []);
  }

}

customElements.define('annotation-panel', AnnotationPanel);
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';

import './annotation-list-item.js';

class TicketListItem extends PolymerElement {
  static get template() {
    return html`
    <style>
    h5,
    annotation-list-item {
      display: block;
      margin: 1rem 2rem;
    }

    .user-info {
      font-size: 1.25rem;
      margin: 1rem 2rem;
    }

    .reason {
      margin: 0.25rem 2rem 0.25rem 2rem;
      font-style: italic;
      color: #888;
      display: inline-block;
    }

    .actions {
      margin-top: 2rem;
      -webkit-display: -webkit-box;
      -webkit-display: -webkit-flex;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      align-items: center;
      justify-content: flex-end;
      width:100%;
      background-color: #eee;
    }
    .actions button {
      flex: none;
    }
    .actions button[active] {
      color:#CA3227;
    }
    .actions button[disabled] {
      background-color: #eee;
    }

    .btn {
      border: none;
      border-radius: 2px;
      margin: 5px 1px;
      text-transform: uppercase;
      letter-spacing: 0;
      will-change: box-shadow, transform;
      transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1), color 0.2s cubic-bezier(0.4, 0, 0.2, 1);
      outline: 0;
      cursor: pointer;
      text-decoration: none;
      background: 0 0;
    }

    .btn:hover {
      color: #333;
      background-color: #e6e6e6;
      border-color: #adadad;
    }

    .btn:disabled {
      opacity: 0.5;
      cursor: not-allowed;
    }

    .btn:disabled:hover {
      background-color: transparent;
      color: inherit;
    }

    button.confirm{
      display: inline-block;
      float:right;
      font-size:12px;
    }

    input[type="text"]:focus {
      outline: 0;
      background-size: 100% 2px,100% 1px;
      transition-duration: .3s;
    }

    input[type="text"] {
      display: inline-block;
      position: relative;
      padding: 2px;
      margin-top: 0.3rem;
      overflow: hidden;
      resize: none;
      border: 0;
      background-image: linear-gradient(#4f4f4f, #4f4f4f), linear-gradient(#D2D2D2, #D2D2D2);
      background-size: 0 2px,100% 1px;
      background-repeat: no-repeat;
      background-position: center bottom, center calc(100% - 1px);
      background-color: rgba(0, 0, 0, 0);
      transition: background 0s ease-out;
      box-shadow: none;
      border-radius: 0;
      font-family: inherit;
      margin-bottom: 1rem;
      margin-left: 2rem;
      width: 70%;
      float: left;
      font-size: 16px;
    }

    details > summary {
      list-style: none;
    }

    details > summary::marker, /* Latest Chrome, Edge, Firefox */
    details > summary::-webkit-details-marker /* Safari */ {
      display: none;
    }
    </style>

    <div class="user-info">
      <span class="user">by [[item.annotations.0.creator]]</span>
      <span class="date">on [[formatDate(item.annotations)]]</span>
      <span class="status">([[formatStatus(item.status)]])</span>
    </div>
    <template is="dom-if" if="[[isStatusRejected(item.status)]]">
      <span class="reason">[[item.annotations.0.reason]]</span>
    </template>
    <template is="dom-if" if="[[hasSplitAnnotation]]">
      <annotation-list-item item="[[splitAnnotation]]">
    </annotation-list-item></template>
    <template id="annotationlist" is="dom-repeat" items="[[item.annotations]]" filter="notTargetingANewWord">
      <annotation-list-item item="[[item]]">
    </annotation-list-item></template>
    <template id="annotationlist2" is="dom-repeat" items="[[item.annotations]]" filter="targettingANewWord">
      <annotation-list-item item="[[item]]">
    </annotation-list-item></template>
    <template is="dom-if" if="[[showActions(item.status, user.role)]]">
    <div class="actions">
      <button class="btn edit" on-click="edit" disabled="[[editingDisabled(item.status, item.creator)]]">edit</button>
      <button class="btn delete" on-click="delete" disabled="[[editingDisabled(item.status, item.creator)]]">delete</button>
      <button class="btn approve" on-click="approve" disabled="[[isDisabled(item.status)]]" hidden="[[isNormalUser(user.role)]]">approve</button>
      <button class="btn reject" on-click="openReasonInput" toggles="" disabled="[[isDisabled(item.status)]]" hidden="[[isNormalUser(user.role)]]">reject</button>
    </div>
    </template>
    <details id="reasonInputContainer">
     <summary></summary>
      <input type="text" id="reasonInput" placeholder="Please enter a reason" tabindex="0"></input>
      <button class="btn confirm" on-click="reject">confirm</button>
    </details>
`;
  }

  static get properties() {
    return {
      item: {
        type: Object,
        notify: true,
        observer: 'itemChanged'
      },
      user: Object,
      splitAnnotation: {
        type: Object,
        computed: 'getSplitAnnotation(item.annotations)'
      },
      hasSplitAnnotation: {
        type: Boolean,
        computed: 'checkSplitAnnotation(splitAnnotation)',
        value: false
      }
    }
  }

  // Don't leave open when moving to a new word.
  // actions
  itemChanged(newVal, oldVal) {
    if (!this.$.reasonInputContainer.open) { return }
    this.$.reasonInput.style.backgroundColor = "transparent";
    this.$.reasonInputContainer.removeAttribute('open');
  }

  edit(event, detail) {
    this.dispatchEvent(new CustomEvent('edit', { bubbles: true, composed: true }));
    this.$.reasonInputContainer.removeAttribute('open');
  }

  delete(event, detail) {
    if (confirm('Really delete?') == false) return;
    this.dispatchEvent(new CustomEvent('delete', { bubbles: true, composed: true }));
    this.$.reasonInputContainer.removeAttribute('open');
  }

  approve(event, detail) {
    this.dispatchEvent(new CustomEvent('approve', { bubbles: true, composed: true }));
    this.$.reasonInputContainer.removeAttribute('open');
  }

  openReasonInput(event) {
    if (this.$.reasonInputContainer.open) { return } // already open
    this.$.reasonInputContainer.setAttribute('open', 'true');
    this.$.reasonInput.focus();
  }

  reject(event, detail) {
    var reason = this.$.reasonInput.value;
    if (this.item.creator !== 'autocorrect'
        && (!reason || reason.value === ''))
    {
      this.$.reasonInput.focus();
      this.$.reasonInput.style.backgroundColor = "#fdd";
      return;
    }
    this.$.reasonInput.style.backgroundColor = "transparent";
    this.$.reasonInputContainer.removeAttribute('open');
    this.dispatchEvent(new CustomEvent('reject', {
      bubbles: true,
      composed: true,
        detail: {
          reason: reason
        }
    }));
    this.$.reasonInput.value = ''
  }

  // computed properties

  getSplitAnnotation(annotations) {
    if (!annotations) return null;
    return annotations.reduce(function (result, annotation) {
      if (annotation.type === 'split') { return annotation }
      return result;
    }, null);
  }

  checkSplitAnnotation(splitAnnotation, b) {
    return (splitAnnotation && 'id' in splitAnnotation);
  }

  editingDisabled(status, creator) {
    return (status === 'accepted' ||
      creator !== this.user.name);
  }

  showActions(status, role) {
    return (status !== 'accepted' && role !== "reader");
  }

  isDisabled(isNew, status) {
    return (status === 'accepted');
  }

  isNormalUser(role) {
    return (role !== 'shcadmin');
  }

  isStatusRejected(status) {
    return status === 'rejected';
  }

  // formatters

  formatStatus(status) {
    switch (status) {
      case 'pending': return 'approval pending';
      case 'accepted': return 'approved';
      case 'rejected': return 'rejected';
      case 'saved': return 'saved';
      default: return 'unknown';
    }
  }

  formatDate(annotations) {
    var date = annotations[0].modifiedDate;
    if (!date) { return '' }
    return date.toDateString();
  }

  visibilityIcon(appliedState) {
    if (appliedState) {
      return 'visibility';
    }
    return 'visibility-off';
  }

  // filters

  notTargetingANewWord(annotation) {
    return (annotation.type !== 'split' &&
      !this.targettingANewWord(annotation));
  }

  targettingANewWord(annotation) {
    var selector = annotation.target.limits[0].getAttribute('value');
    return (/-s-3$/.test(selector));
  }
}

customElements.define('ticket-list-item', TicketListItem);

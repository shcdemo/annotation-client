import '@dile/dile-tabs/dile-tabs.js';
import '@dile/dile-pages/dile-pages.js';

import './annotation-panel.js';
import './annotatable-document.js';

// atomic elements
import './tei-w.js';
import './tei-c.js';
import './tei-pc.js';


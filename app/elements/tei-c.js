import { AtomicNode } from './atomic-node.js';
import { Annotation } from './annotation.js';

class TeiC extends AtomicNode {
  static get is() {
    return 'tei-c';
  }
  static get behaviors() {
    return [Annotation.atomic.actions];
  }
}

customElements.define(TeiC.is, TeiC);

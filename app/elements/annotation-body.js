import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { Annotation } from './annotation.js';

export class AnnotationBody extends PolymerElement {

  /*

  Example:

  <annotation-body
    type="TEI"
    subtype="regularization"
    format="text/xml">
    regularized
  </annotation-body>

  */
 
  parseDocumentFragment(type, doc) {
    var parser = new DOMParser()
    var fragment = parser.parseFromString(doc, "application/xml")
    if (fragment.getElementsByTagName('parsererror').length) {
      console.error(fragment)
      return {
        text: ' - not parseable - '
      }
    }
    var body = fragment.firstChild
    if (Annotation.Corrections.indexOf(type) >= 0) {
      var contents = [].slice.call(body.getElementsByTagName('w'))
      var text = contents.map(function (element) {
        return element.innerHTML
      }).join(' ')
      return {
        text: text
      }
    }
    if (type === Annotation.ActionTypes.UPDATE_ATTRIBUTE) {
      var attribute = body.querySelector('attribute')
      return {
        attributeName: attribute.getAttribute('name'),
        text: attribute.innerHTML.trim()
      }
    }
    if (type === Annotation.ActionTypes.COMMENT ||
      type === Annotation.ActionTypes.NOTE) {
      return {
        text: body.innerHTML.trim()
      }
    }
  }

  textToDocumentFragment(type, data) {
    if (type === Annotation.ActionTypes.UPDATE) {
      return '<w>' + data.body + '</w>'
    }
    if (type === Annotation.ActionTypes.JOIN) {
      return '<w>' + data.body + '</w>'
    }
    if (type === Annotation.ActionTypes.DELETE) {
      return '<w>' + data.body + '</w>'
    }
    if (type === Annotation.ActionTypes.SPLIT) {
      var words = data.body.split(' ')
      return words.map(function (word) {
        return '<w>' + word + '</w>'
      }).join('<c> </c>')
    }
    if (type === Annotation.ActionTypes.UPDATE_ATTRIBUTE) {
      return '<attribute name="' + data.attributeName + '">'
        + data.body + '</attribute>'
    }
    if (type === Annotation.ActionTypes.COMMENT ||
      type === Annotation.ActionTypes.NOTE) {
      return data.body
    }
  }

  static get behaviors() {
    return Annotation.ActionTypes;
  }

  static get properties() {
    return {
      subtype: {
        type: String,
        value: 'unknown',
        reflectToAttribute: true,
        observer: '_setFormatForSubtype'
      },
      type: {
        value: 'TEI',
        reflectToAttribute: true
      },
      format: {
        value: 'text/xml',
        reflectToAttribute: true
      },
      originalValue: {
        value: '--'
      },
      display: {
        value: '',
        reflectToAttribute: true
      }
    }
  }

  factoryImpl(data) {
    if (!this._isAllowedType(data.action)) {
      console.info('annotation type', data.action, 'not supported, yet')
      this.remove()
      return false
    }
    this._setContent(data)
  }

  _isAllowedType(type) {
    return (this.allowedTypes.indexOf(type) >= 0);
  };

  constructor(data) {
    super();
    this.allowedTypes = Annotation.ActionTypes.ALL;
    Annotation.Body = AnnotationBody;
    if (data) {
      this.factoryImpl(data);
    }
  }

  connectedCallback() {
    super.connectedCallback();
    this.dispatchEvent(new CustomEvent('annotation-body-attached',
    {
      bubbles: true,
      composed: true,
      detail: {
        bodyReady: true
      }
    }));
    if (!this._isAllowedType(this.subtype)) {
      console.error('unsupported annotation type', this.subtype);
    }

    if (this.display) { return }
    // build display property value from contents
    // use outerHTML to prevent DomParser errors (ensure root node)
    var data = this.parseDocumentFragment(this.subtype, this.outerHTML);
    this.display = data.text;
    this.data = data;
  }

  _getContent() {
    return this.textToDocumentFragment(this.subtype, this.data);
  }

  _setContent(data) {
    this.data = data;
    this.subtype = data.action;
    this.display = data.body;
    this.originalValue = data.originalValue;
    this.innerHTML = this._getContent();
  }

  _setFormatForSubtype(newValue) {
    if (newValue === Annotation.ActionTypes.COMMENT ||
      newValue === Annotation.ActionTypes.NOTE) {
      this.format = 'text/plain';
      return;
    }
    this.format = 'text/xml';
  }
}

customElements.define('annotation-body', AnnotationBody);
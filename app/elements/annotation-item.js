import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { FlattenedNodesObserver as PolymerFlattenedNodesObserver } from '@polymer/polymer/lib/utils/flattened-nodes-observer.js';
import { dom } from '@polymer/polymer/lib/legacy/polymer.dom.js';

import { AnnotationBody } from './annotation-body.js';
import { AnnotationTarget } from './annotation-target.js';
import { Annotation } from './annotation.js';

export class AnnotationItem extends PolymerElement {

  /*
  
  Example:
  
  <annotation-item id="A4ade5a62-0c87-11e9-91d6-ed949fd91804" 
                   generator="earlyPrint"
                   status="pending"
                   visibility="public"
                   reason=""
                   creator="autocorrect"
                   created="2018-12-30T17:04:44.303Z"
                   modifier="autocorrect"
                   modified="2018-12-30T17:04:44.303Z"
                   generated="2018-12-30T17:04:44.303Z"
                   ticket="s-1546211084303">
       <annotation-body original-value="y●" subtype="update" format="text/xml" type="TEI">
           <w>yf</w>
       </annotation-body>
       <annotation-target source="A03936" version="">
           <target-selector type="IdSelector" value="A03936-141-b-0430"/>
       </annotation-target>
  </annotation-item>
  */

  static get properties() {
    return {
      id: String,
      applied: { // is this change applied to the document?
        type: Boolean,
        value: false,
        notify: true
      },
      creator: {
        type: String,
        reflectToAttribute: true
      },
      creatorName: {
        type: String,
        // will be called whenever creator changes
        computed: 'computeCreatorName(creator)'
      },
      generator: {
        type: String,
        value: 'earlyPrint',
        reflectToAttribute: true
      },
      createdDate: {
        type: Date,
        value: function () {
          var created = this.getAttribute('created');
          return new Date(created);
        }
      },
      modifiedDate: {
        type: Date,
        computed: 'computeModifiedDate(modified)'
      },
      modified: {
        type: Date,
        reflectToAttribute: true
      },
      generated: {
        type: Date,
        reflectToAttribute: true
      },
      status: {
        value: 'unkown',
        type: String,
        reflectToAttribute: true
      },
      visibility: {
        value: 'unkown',
        type: String,
        reflectToAttribute: true
      },
      tempId: {
        type: String,
        reflectToAttribute: true,
        value: ''
      },
      sent: Boolean,
      // computed from distributed children
      target: Object,
      body: Object,
      reason: {
        value: '',
        type: String,
        reflectToAttribute: true
      },
      original: {
        value: '',
        type: String,
        notify: true
      },
      ticket: {
        type: String,
        reflectToAttribute: true
      }
    }
  }

  factoryImpl (data) {
    console.log('create annotation from data', data)
    var now = new Date()
    this.tempId = 'tmp-' + now.getTime()
    this.creator = data.name // app.user.name
    this.createdDate = now
    this.setAttribute('created', now.toISOString())
    this.modified = now.toISOString()
    this.generated = now.toISOString()
    this.ticket = data.ticket
    this.status = 'pending'
    this.visibility = data.visibility || 'public'
    this.type = data.action
    this.body = new AnnotationBody(data)
    dom(this).appendChild(this.body)

    this.target = new AnnotationTarget(data)
    dom(this).appendChild(this.target)
  }

  computeCreatorName(newValue) {
    if (!newValue) { return '' }
    // user URL
    var matches = newValue.match(/\/([^\/]+)$/);
    if (matches) {
      console.log('user URL', newValue);
      return matches[1]
    }
    // user ID
    return newValue;
  }

  computeModifiedDate(newDate) {
    if (!newDate) return new Date();
    return new Date(newDate);
  }

  initTarget() {
    this.target = this._myQueryEffectiveChildren('annotation-target');
    this.target.addEventListener('attached', event => this._setContentAndApply);
  }

  initBody() {
    // allow only a single body per annotation
    this.body = this._myQueryEffectiveChildren('annotation-body');
    this.type = this.body.getAttribute("subtype");
    if (!this.body) {
      console.info('Bodyless Annotation');
    }
  }

  constructor(data) {
    super();
    if (data) {
      this.factoryImpl(data);
    }
    this.addEventListener('tap', event => this.markTarget(event));
  }

  ready() {
    super.ready();
    Annotation.Item = AnnotationItem;
    this._affected = [];
  }

  connectedCallback() {
    super.connectedCallback();
    if (!this.ticket) {
      this.ticket = 's-' + this.id;
    }
    this.initTarget();
    this.initBody();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    if (this.applied) {
      this.apply(); // reset changes before removal
    }
    this._removeReferences();
    if (this.body.subtype === Annotation.ActionTypes.UPDATE_ATTRIBUTE
        && this.body.querySelector('attribute').getAttribute('name') === 'defect') {
      this.markAsDefective();
    }
    this.dispatchEvent(new CustomEvent('annotation-detached', {
       bubbles: true,
       composed: true,
       detail: {
        data: this.id
       }
      }));
  }
  _setContentAndApply() {
    this.original = this.getOriginalContent();
    this._affected = Annotation.Selection.getElementsInRange(this.target.getTargets());
    if (this.body.subtype === Annotation.ActionTypes.UPDATE_ATTRIBUTE
        && this.body.querySelector('attribute').getAttribute('name') === 'defect') {
      this.markAsDefective(this.body.display);
    }

    if (this.status !== 'rejected') {
      switch (this.body.subtype) {
          case Annotation.ActionTypes.UPDATE:
          case Annotation.ActionTypes.SPLIT:
              this.setApprovedContent(this.body.display);
              break;
           case Annotation.ActionTypes.JOIN:
              this.setApprovedJoinContent(this.body.display);
              break;
      }
      this.apply();
    }

    this._addReferences();
    this.dispatchEvent(new CustomEvent('annotation-attached', { data: this, bubbles: true, composed: true }));
  }

  _addReferences() {
    this._affected.map(function (element) {
      if (element == null) { return }
      var target = element;
      if ('orig' in element) {
        target = element.orig;
      }
      target.addReference(this);
      target._setStyle();
    }, this);
  }

  _removeReferences() {
    this._affected.map(function (element) {
      var target = element;
      if ('orig' in element) {
        target = element.orig;
      }
      target.removeReference(this);
      target._setStyle();
    }, this);
  }

  apply() {
    var limits = this.target.getTargets();
    if (!limits || !limits[0]) { return }
    var content = (this.applied
                  ? this.body.originalValue
                  : this.body.display) || '';
    console.log('apply', this.id, this.body.subtype, limits, content);
    // get list of targets
    switch (this.body.subtype) {
      case Annotation.ActionTypes.UPDATE:
        this._affected = this.updateWord(limits, content);
        break;
      case Annotation.ActionTypes.SPLIT:
        this._affected = this.splitWord(limits, content);
        break;
      case Annotation.ActionTypes.JOIN:
        this._affected = this.joinWords(limits, content);
        break;
      case Annotation.ActionTypes.UPDATE_ATTRIBUTE:
        this._affected = this.updateAttribute(limits, content);
        break;
      default:
        this._affected = Annotation.Selection.getElementsInRange(limits);
    }
    if (this.applied) {
      console.log('reset annotation for', this.id);
      return this._reset();
    }
    this.set('applied', true);
    if (this.status === 'accepted') {
      this.markAsResolved();
    }
    this._addReferences();
  }

  getOriginalContent() {
    var limits = this.target.getTargets();
    if (limits[0] == null) {
      return 'N/A';
    }
    if (this.body.subtype === Annotation.ActionTypes.UPDATE_ATTRIBUTE) {
      var attribute = this.body.querySelector('attribute').getAttribute('name');
      return limits[0].getAttribute(attribute);
    }
    var elementsInRange = Annotation.Selection.getElementsInRange(limits);
    return elementsInRange.reduce(Annotation.textReducer, '');
  }

  markAsDefective(state) {
    this._affected.forEach(function (element) {
      var target = element;
      if ('orig' in element) {
        target = element.orig;
      }
      target.setAttribute('defect', state);
      target.defect = state;
    })
  }

  markAsResolved() {
    this._affected.forEach(function (element) {
      var target = element;
      if ('orig' in element) {
        target = element.orig;
      }
      target._setStyle();
    })
  }

  setApprovedContent(content) {
    this._affected.forEach(function (element) {
      var target = element;
      target.innerHTML = content;
      if (target.hasAttribute("reg")) {
          target.setAttribute("orig", content);
      }
    });
  }

   setApprovedJoinContent(content) {
    this._affected.forEach(function (element, index) {
      var target = element;
      if (index === 0) {
          target.innerHTML = content;
      }
      else {
          target.setAttribute("hidden", true);
      }
    });
  }

  getContent() {
    return this.body._getContent(this.target.getTargets());
  }

  _reset() {
    this._affected.forEach(function (target) {
      if (target.id.match(/^\tmp\-/)) {
        target.remove();
      }
    })
    this.set('applied', false);
  }

  _myQueryEffectiveChildren(selector) {
    const x = PolymerFlattenedNodesObserver
                .getFlattenedNodes(this)
                .filter(n => n.nodeType === Node.ELEMENT_NODE)
                .filter(n => n.parentNode.querySelector(selector) === n);
    return x.length > 0 && x[0] || [];
  }

  splitWord (targets, content) {
    // update first element textNode with first word
    var first = targets.shift();
    var words = content.split(' ');
    first.innerHTML = words.shift();

    var affected = [first];
    var ref = first.nextElementSibling;
    var parent = first.parentElement;

    if (words.length === 0) {
      // we're deleting, so remove fake elements we inserted
      while (ref.getAttribute('id').match(/^tmp-/)) {
        var elementToRemove = ref;
        ref  = elementToRemove.nextElementSibling;
        elementToRemove.remove();
      }
    }
    else {
      // add tei-c and tei-w elements
      words.reverse().forEach(function (word) {
        var wordElement = document.createElement('tei-w');
        wordElement.id = Annotation.createTempId();
        var t = document.createTextNode(word);
        wordElement.appendChild(t);
        parent.insertBefore(wordElement, ref);
        affected.push(wordElement);

        var space = document.createElement('tei-c');
        space.id = Annotation.createTempId();
        space.appendChild(document.createTextNode(' '));
        parent.insertBefore(space, wordElement);
        affected.push(space);

        // set new reference element to insert before
        ref = space;
      })
    }
    return affected;
  }

  joinWords (targets, content) {
    // update first element textNode with first word
    var first = targets.shift();
    var affected = [first];
    first.innerHTML = content;

    var followingSpace = first.nextElementSibling;
    if (followingSpace && followingSpace.nodeName === 'TEI-C') {
      followingSpace.remove();
      affected.push(followingSpace);
    }
    // delete the rest
    targets.forEach(function (element, index, array) {
      if (element) {
        element.remove();
        affected.push(element);

        if (index === array.length - 1) { return }

        var nextElementSibling = element.nextElementSibling;
        nextElementSibling.remove();
        affected.push(nextElementSibling);
      }
    })
    // add again
    return affected;
  }

  updateWord (targets, content) {
    targets[0].innerHTML = content;
    return targets;
  }

  updateAttribute (targets, content) {
    var attribute = this.body.querySelector('attribute').getAttribute('name');
    targets[0].setAttribute(attribute, content);
    if (this.body.data) {
        this.body.data.attributeName = attribute;
    }
    else {
        this.body.data = { attributeName: attribute };
    }
    return targets;
  }


}

customElements.define('annotation-item', AnnotationItem);

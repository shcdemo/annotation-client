import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';
import {} from '@polymer/polymer/lib/elements/dom-if.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';

import { Annotation } from './annotation.js';
import { AnnotationItem } from './annotation-item.js';
import './ticket-list-item.js';

class AnnotationList extends PolymerElement {
  static get template() {
    return html`
    <style>
      :host {
        display: block;
        box-sizing: border-box;
        padding: 0;
        border: inherit;
      }

      .ticket-list--title {
        margin: 1rem 1.75rem;
      }
      .ticket-list--actions {
        margin: 0 0.75rem;
      }

      .loader, .count {
        float: left;
        padding-right:6px;
      }

      ul {
        list-style-type: none;
        list-style-position: inside;
        padding-left: 0;
      }

      li {
        display: block;
      }
      /* indicate annotation status */
      li.status-unknown {
        border-top: 3px solid #888;
      }
      li.status-accepted {
        border-top: 3px solid #bbd5b5;
      }
      li.status-pending {
        border-top: 3px solid #fff7a9;
      }
      li.status-rejected {
        border-top: 3px solid salmon;
      }

      @-webkit-keyframes highlight-fade {
        0% {
          background: orange;
        }
        100% {
          background: white;
        }
      }
      @-moz-keyframes highlight-fade {
        0% {
          background: orange;
        }
        100% {
          background: white;
        }
      }
      @-ms-keyframes highlight-fade {
        0% {
          background: orange;
        }
        100% {
          background: white;
        }
      }
      @keyframes highlight-fade {
        0% {
          background: orange;
        }
        100% {
          background: white;
        }
      }
      .highlightAnno {
        -webkit-animation: highlight-fade 2s ease-out;
        -moz-animation: highlight-fade 2s ease-out;
        -o-animation: highlight-fade 2s ease-out;
        animation: highlight-fade 2s ease-out;
      }

      .btn-block {
        display: block;
        width: 100%;
        height: 24px;
      }

      .btn {
        border: none;
        border-radius: 2px;
        margin: 5px 1px;
        text-transform: uppercase;
        letter-spacing: 0;
        will-change: box-shadow, transform;
        transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1), color 0.2s cubic-bezier(0.4, 0, 0.2, 1);
        outline: 0;
        cursor: pointer;
        text-decoration: none;
        background: 0 0;
      }

      .btn:hover {
        color: #333;
        background-color: #e6e6e6;
        border-color: #adadad;
      }

      .btn:disabled {
        opacity: 0.5;
        cursor: not-allowed;
      }

      .btn:disabled:hover {
        background-color: transparent;
        color: inherit;
      }
    </style>

    <div class="ticket-list--actions" hidden="[[isReader(user.role)]]">
      <button class="btn" on-click="handleTicketEdit">add annotation</button></div>
    <h4 class="ticket-list--title">[[title]]
      <span class="count" hidden="[[loading]]">{{itemCount}}</span>
      <span class="loader" hidden="[[!loading]]">
        <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="30px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
          <path fill="#666" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
            <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 25 25" to="360 25 25" dur="1.0s" repeatCount="indefinite"></animateTransform>
            </path>
          </svg>
      </span>
    </h4>
    <ul class="display-list">
      <template id="list" is="dom-repeat" items="{{tickets}}" filter="filterList" sort="sortList" on-rendered-item-count-changed="_setItemCount">
        <li class$="[[getClassesForTicket(item.status, item.new)]]">
          <ticket-list-item item="[[item]]" user="[[user]]" on-delete="handleTicketDelete" on-edit="handleTicketEdit" on-approve="handleTicketApprove" on-reject="handleTicketReject">
          </ticket-list-item>
        </li>
      </template>
    </ul>

`;
  }

  static get properties() {
    return {
      server: String,
      title: {
        type: String,
        value: 'Tickets'
      },
      user: {
        type: Object,
        value: function () { return {} }
      },
      annotations: {
        type: Array,
        value: function () { return [] }
      },
      tickets: {
        type: Array,
        value: function () { return [] }
      },
      showTickets: {
        type: Array,
        value: function () { return [] }
      },
      loading: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },
      hidden: {
        type: Boolean,
        value: true
      },
      itemCount: Number,

      // ticket list sorting
      _sorting:  {
        type: Array,
        value: function () { return [] }
      }
    }
  }

  /**
   * helper function used in Array.map to retrieve a list of IDs
   */
  getId(element) {
    return element.id;
  }

  // ticket list filtering

  filterByTickets(tickets) {
    var ticketIds = tickets.map(function (a) {
      return a.ticket;
    })
    this.filterList = function (ticket) {
      return (ticketIds.indexOf(ticket.id) >= 0 && this.defaultFilter(ticket));
    }
    this.$.list.render();
  }

  sortList(x, y) {
    return this.sortByMultiple(x, y, this._sorting);
  }

  sortByCreatedDate(a, b) {
    var aTime = a.createdDate.getTime();
    var bTime = b.createdDate.getTime();
    if (aTime === bTime) { return 0 }
    return (aTime < bTime ? 1 : -1);
  }

  sortByTargetElementPosition(a, b) {
    var aFirstTargetElementYPos = a.target.getTargets()[0].getBoundingClientRect().top;
    var bFirstTargetElementYPos = b.target.getTargets()[0].getBoundingClientRect().top;
    if (aFirstTargetElementYPos === bFirstTargetElementYPos) { return 0 }
    return (aFirstTargetElementYPos > bFirstTargetElementYPos ? 1 : -1);
  }

  sortByMultiple(a, b, sortFuncs) {
    return sortFuncs.reduce(function (result, sortFunc) {
      if (result !== 0) { return result }
      return sortFunc(a, b);
    }, 0);
  }


  /**
   * handle for added annotation
   * needs to be bound to a annotation-list
   */
  handleAnnotationAttachedEvent(event) {
    this.push('annotations', event.target);
    event.target.removeEventListener('annotation-attached', this._boundAttachedHandler);
  }

  /**
   * handle removed annotation
   * needs to be bound to a annotation-list
   */
  handleAnnotationDetachedEvent(event) {
    this.removeAnnotationFromList(event.detail.data);
  }

  addAnnotationItemListeners(annotation) {
    annotation.addEventListener('annotation-attached', this._boundAttachedHandler);
    annotation.addEventListener('annotation-detached', this._boundDetachedHandler);
  }

  /**
   * add new annotations that were created elsewhere
   * with document.createElement('<annotation-item />')
   * or jQuery('<annotation-item />') - use get(0) to access the raw element
   */
  addAnnotationElement(newAnnotation) {
    this.addAnnotationItemListeners(newAnnotation);
    var slotDiv = this.parentNode.host.querySelector("div[slot='annotations']");
    if (slotDiv) { slotDiv.appendChild(newAnnotation); }
    newAnnotation.apply();
    this.push('annotations', newAnnotation);
  }

  static get observers() {
    return [
      'compileTickets(annotations.*)'
    ];
  }

  connectedCallback() {
    super.connectedCallback();
    // Poor man's export for filterList and resetFilters since Polymer can't do
    // standard ES6 exports.
    this.filterList = this.defaultFilter;
    this.resetFilters = function() {
      this.filterList = this.defaultFilter;
      this.$.list.render();
    };

    this._boundAttachedHandler = this.handleAnnotationAttachedEvent.bind(this);
    this._boundDetachedHandler = this.handleAnnotationDetachedEvent.bind(this);

    var annotations = this.parentNode.host.querySelectorAll('annotation-item');
    annotations.forEach((anno) => {
      this.addAnnotationItemListeners(anno);
    });

    // In Polymer 3, observers fire too late to do any good at initialization time,
    // so fake up a change object that will load up any tickets that already exist
    // on this page.

    var cr = Object.assign({}, {
      base: Array.from(annotations),
      path: "annotations.splices",
      value: {
        indexSplices: [
          {
            index: 0,
            addedCount: annotations.length,
            removed: [],
            Object: [],
            type: "splice"
          }
        ]
      }
    });
    this.compileTickets(cr);

  }

  async _doRequest(type, urlSuffix, body) {

    const method = type === 'delete' ? 'GET': 'POST';
    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    if (protocol === 'http:' && port.length > 0 && port != '80'
      || protocol === 'https:' && port.length > 0 && port != '443') {
      host += ':' + port;
    }
    var base = protocol + '//' + host;
    const url = new URL(this.server + '/' + type + '/' + urlSuffix, base);

    const baseOptions = {
      method: method,
      mode: 'cors',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/xml'
      }
    };

    const addBody = { body: body };
    const options =  method === 'POST' ? Object.assign(baseOptions, addBody) : baseOptions;

    return await fetch(url, options)
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.text();
      })
      .then(xmlStr => {
        const parser = new DOMParser();
        const xmlDoc = parser.parseFromString(xmlStr, 'text/xml');
        return xmlDoc;
      })
      .catch((error) => {
        console.error(`Error in fetch: ${error}`);
        throw error;
      });
  }

  // manipulate annotation list methods

  addAnnotation(data) {
    var newAnnotation = new AnnotationItem(Object.assign({}, data, this.user));
    newAnnotation.addEventListener('annotation-detached', this._boundDetachedHandler);
    var slotDiv = this.parentNode.host.querySelector("div[slot='annotations']");
    if (slotDiv) {
      slotDiv.appendChild(newAnnotation);
    }
    this.push('annotations', newAnnotation);

    var index = this.annotations
      .map(function (e) { return e.tempId })
      .indexOf(newAnnotation.tempId);

    function applyChanges(index, resultItem) {
      console.log('in apply changes');
      console.log(this);
      console.log(index);
      console.log(resultItem);
      // update client with response
      this.annotations[index].id = resultItem.id;
      this.annotations[index].tempId =  null;
      this.annotations[index].body.originalValue = resultItem.originalValue;
      this.annotations[index].status = resultItem.status;
      this.annotations[index]._setContentAndApply();
    }

    function handleAnnotationCreateResponse(index, response) {
      var resultItem = response.querySelector('annotation-item');
      if (!resultItem || !resultItem.id) {
        console.error(response);
        throw Error('the server did not respond properly');
      }
      return {
        id: resultItem.id,
        status: resultItem.getAttribute('status'),
        originalValue: resultItem.querySelector('annotation-body')
          .getAttribute('original-value')
      }
    }

    function handleAnnotationCreateError(index, error) {
      console.error(error);
      alert('could not create annotation: ' + error);
      return false;
    }

    if (this.server === '' || Object.keys(this.server).length === 0) {
      var fakeRequest = new Promise(function (resolve, reject) {
        resolve({
          id: newAnnotation.tempId,
          status: 'pending',
          originalValue: data.originalValue
        });
      });
      return fakeRequest.then(applyChanges.bind(this, index));
    }

    // communicate to server

    return this._doRequest('create', '', newAnnotation.outerHTML)
    .then(response => {
      return handleAnnotationCreateResponse(index, response);
    })
    .then(newItem => {
      console.log('newItem:', newItem);
      applyChanges.call(this, index, newItem);
    })
    .catch(error  => {
      handleAnnotationCreateError(index, error);
    });
  }

  addAnnotations(newAnnotations) {
    var slotDiv = this.parentNode.host.querySelector("div[slot='annotations']");
    if (slotDiv) {
      slotDiv.innerHTML = "";
    }
    newAnnotations.forEach(anno => {
      this.addAnnotationElement(anno);
    });
  }

  updateAnnotation(annotationData) {
    var index = this.annotations
      .map(this.getId)
      .indexOf(annotationData.id);

    var annotationToUpdate = this.annotations[index];

    annotationToUpdate.body._setContent(annotationData);
    var modifiedDate = new Date();
    annotationToUpdate.modified = modifiedDate.toISOString();
    annotationToUpdate.visibility = annotationData.visibility;

    function handleUpdateAnnotationResponse(index, request) {
      this.annotations[index].applied = false;
      this.annotations[index].apply();
      this.notifyPath(['annotations', index, 'modified']);
      this.notifyPath(['annotations', index, 'body', 'display']);
      this.notifyPath(['annotations', index]);
      return true;
    }

    function handleUpdateAnnotationError(index) {
      alert('could not update annotation', annotationData.id);
      this.notifyPath(['annotations', index, 'modified']);
      this.notifyPath(['annotations', index, 'body', 'display']);
      this.notifyPath(['annotations', index]);
      return false;
    }

    // communicate to server
    if (this.server === '' || Object.keys(this.server).length === 0) {
      return handleUpdateAnnotationResponse.bind(this, index)();
    }

    return this._doRequest( 'update', '', annotationToUpdate.outerHTML)
      .then(handleUpdateAnnotationResponse.bind(this, index))
      .catch(handleUpdateAnnotationError.bind(this, index));
  }

  deleteAnnotation(annotationToRemove) {
    var indexToRemove = this.annotations.map(this.getId).indexOf(annotationToRemove.id);

    var handleDeleteSuccess = function () {
      // this has to be called explicitly
      annotationToRemove.remove();
      return true;
    }

    // store status quo
    var oldStatus = annotationToRemove.status;
    var oldVisibility = annotationToRemove.visibility;

    // mark for deletion
    this.annotations[indexToRemove].status = 'unknown';
    this.annotations[indexToRemove].visibility = 'unknown';
    var urlPath = annotationToRemove.target.getAttribute('source') + '/' + annotationToRemove.id;

    var handleDeleteError = function () {
      alert('There was a problem on the server! Could not delete annotation with path "' + urlPath + '"');
      this.annotations[indexToRemove].status = oldStatus;
      this.annotations[indexToRemove].visibility = oldVisibility;
      return false;
    }

    // skip server call for unsubmitted annotations
    if (this.server === '' || Object.keys(this.server).length === 0 || !annotationToRemove.id) {
      return handleDeleteSuccess.bind(this)();
    }

    // communicate to server

    return this._doRequest('delete',
                           urlPath,
                           annotationToRemove.outerHTML)
      .then(handleDeleteSuccess.bind(this))
      .catch(handleDeleteError.bind(this));
  }

  removeAnnotationFromList(id) {
    var indexToRemove = this.annotations.map(this.getId).indexOf(id);
    if (indexToRemove < 0) return;
    this.splice('annotations', indexToRemove, 1);
  }

  // wipe entire annotations array - e.g. on page switch
  empty() {
    this.set('annotations', []);
  }

  _updateAnnotationStatus(annotationToUpdate, newStatus) {
    var index = this.annotations.map(this.getId).indexOf(annotationToUpdate.id);
    var oldStatus = this.get(['annotations', index, 'status']);
    this.annotations[index].status = newStatus;

    function handleAnnotationUpdateStatusResponse(request) {
      return true;
    }

    function handleAnnotationUpdateStatusError(error) {
      alert(
        'could not change status of annotation ' + this.annotations[index].id
      );
      console.info('<annotation-list>._updateAnnotationStatus Error:', error);
      this.annotations[index].status = oldStatus;
      return false;
    }

    if (!this.server || Object.keys(this.server).length === 0) {
      handleAnnotationUpdateStatusResponse.call(this, null);
      return Promise.resolve(true);
    }

    return this._doRequest('review', '', annotationToUpdate.outerHTML)
      .then(handleAnnotationUpdateStatusResponse.bind(this))
      .catch(handleAnnotationUpdateStatusError.bind(this));
  }

  // ticket event handlers

  handleTicketDelete(event) {
    var annotations = event.model.item.annotations;
    if (!annotations) return;
    annotations.forEach(this.deleteAnnotation.bind(this));
    this.dispatchEvent(new CustomEvent('cancel.selection', { bubbles: true, composed: true }));
  }

  handleTicketEdit(event) {
    var data = {}
    if ('model' in event) {
      data.annotations = event.model.item.annotations;
    }
    var customEvent = new CustomEvent('edit', { detail: {data: data} });
    this.dispatchEvent(customEvent);
  }

  handleTicketApprove(event) {
    var annotations = event.model.item.annotations;
    var ticketIndex = this.tickets.map(this.getId)
      .indexOf(event.model.item.id);

    this.set(['tickets', ticketIndex, 'status'], 'accepted');

    annotations.forEach(annotation => {
      try {
        this._updateAnnotationStatus(annotation, 'accepted');
      }
      catch (error) {
        console.error('Something went wrong during approval', error);
      }
    });

    this.dispatchEvent(new CustomEvent('cancel.selection', { bubbles: true, composed: true }));
  }

  handleTicketReject(event) {
    var annotations = event.model.item.annotations;
    var ticketIndex = this.tickets.map(this.getId)
      .indexOf(event.model.item.id);

    this.set(['tickets', ticketIndex, 'status'], 'rejected');

    annotations.forEach(annotation => {
      try {
        annotation.reason = event.detail.reason;
        this._updateAnnotationStatus(annotation, 'rejected');
      }
      catch (error) {
        console.error('Something went wrong during rejection', error);
      }
    });

    this.dispatchEvent(new CustomEvent('cancel.selection', { bubbles: true, composed: true }));
  }

  // ticket display helper functions

  getClassesForTicket(status, highlight) {
    var classList = ['status-' + status];
    if (highlight) { classList.push('highlightAnno') }
    return classList.join(' ');
  }

  isReader(role) {
    return (role === 'reader');
  }

  open() {
    this.style.display = "block";
  }

  close() {
    this.style.display = "none";
  }

  _setItemCount() {
    this.set('itemCount', this.$.list.renderedItemCount);
    return;
  }

  /**
   * filter imported annotations
   */
  defaultFilter(ticket) {
    return ticket.annotations[0].creator !== 'earlyPrint';
  }

  getAnnotationsForTicket(ticketId) {
    var results = this.tickets.filter(function (ticket) {
      return ticket.id === ticketId;
    });
    if (results.length === 0) {
      return [];
    }
    return results[0].annotations;
  }

  // ticket list compilation

  compileTickets(cr) {
    if (!cr) { return }
    var pathMatches = cr.path.match(/^annotations\.(#?)([^\.]+)\.?(.*)+/);
    if (pathMatches && pathMatches[1] === '' && pathMatches[3]) {
      var subProperty = pathMatches[3];
      var changedAnnotation = this.get('annotations.' + pathMatches[1] + pathMatches[2]);
      console.debug(subProperty, 'of', changedAnnotation.id, 'changed');
      var ticketIndex = this.tickets
        .map(this.getId)
        .indexOf(changedAnnotation.ticket);
      var annotationTicketIndex = this.tickets[ticketIndex].annotations
        .map(this.getId)
        .indexOf(changedAnnotation.id);
      var subPropertyPath = [
        'tickets', ticketIndex,
        'annotations', annotationTicketIndex, subProperty
      ];
      if (subProperty === 'id') {
        this.set(['tickets', ticketIndex, 'new'], false);
      }
      return this.notifyPath(subPropertyPath);
    }

    if (cr.path !== 'annotations' && pathMatches[2] !== 'splices') { return }

    // rebuild entire ticket list
    var tickets = cr.base.reduce(function (result, element) {
      var ticket = element.ticket;
      if (!result[ticket]) {
        result[ticket] = [];
      }
      result[ticket].push(element);
      return result;
    }, {});

    var ticketArray = []
    for (var ticketId in tickets) {
      ticketArray.push({
        id: ticketId,
        creator: tickets[ticketId][0].creator,
        status: tickets[ticketId][0].status,
        new: !!tickets[ticketId][0].tempId,
        annotations: tickets[ticketId]
      });
    }
    this.set('tickets', ticketArray);
    this.notifyPath('tickets');
  }

}

customElements.define('annotation-list', AnnotationList);
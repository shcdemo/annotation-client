import { AtomicNode } from './atomic-node.js';
import { Annotation } from './annotation.js';

class TeiW extends AtomicNode {
  static get is() {
    return 'tei-w';
  }
  static get behaviors() {
    return [
      Annotation.atomic.actions,
      Annotation.itemEditable,
      Annotation.itemSelectable
    ];
  }
}

customElements.define(TeiW.is, TeiW);

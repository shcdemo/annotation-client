import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
/*
  Styles, actions and more for atomic nodes
  eg words(w), whitespace(c) and punctuation(pc)
*/

export class AtomicNode extends PolymerElement {

  static get template() {
    return html `
  <template>
    <style>
      :host {
        display: inline;
        position: relative;
      }
    </style>
  </template>
`;
  }

  static get properties() {
    return {
      atomic: {
        value: true,
        type: Boolean
      },
      selected: {
        type: Boolean,
        value: false,
        observer: '_selectChanged'
      },
      highlight: {
        type: Boolean,
        value: true,
        observer: '_highlightChanged'
      },
      defect: {
        type: Boolean,
        value: false
      },
      resolved: {
        type: Boolean,
        value: false
      },
      tickets: {
        type: Array,
        value: function () { return [] }
      }
    }
  }

  connectedCallback() {
    this.toggleClass('atomic', true);
    this.toggleClass('selectable', true);
  }

  _highlightChanged(newValue) {
    this.toggleClass('selectable', newValue);
    this._setStyle();
  }

  _selectChanged(newValue) {
    this.toggleClass('selected', newValue);
    this._setStyle();
  }

  _setStyle() {
    this.style.backgroundColor = this.getBgColor();
    this.style.textDecoration = this.getTextDecoration();
  }

  addReference(element) {
    var ids = this.tickets.map(function (e) { return e.id });
    if (ids.indexOf(element.id) >= 0) { return }
    this.push('tickets', element);
  }

  removeReference(element) {
    var ids = this.tickets.map(function (e) { return e.id });
    var index = ids.indexOf(element.id);
    if (index < 0) { return }
    this.splice('tickets', index, 1);
  }

  getBgColor() {
    if (!this.highlight) {
      return '';
    }
    if (this.selected) {
      return 'lightblue';
    }
    if (this.defect) {
      return 'salmon';
    }
    if (!this.tickets) { return '' }

    return this.tickets.reduce(function (b, annotation) {
      if (annotation.status === 'accepted' && annotation.creator !== 'earlyPrint' && b === '') {
        return '#bbd5b5';
      }
      if (annotation.status === 'pending' && b === '') {
          if (annotation.creator === 'autocorrect') {
            return '#f2ba05';
          }
          else {
            return '#fff7a9';
          }
      }
      if (annotation.status === 'rejected') {
        return 'salmon';
      }
      return b;
    }, '')
  }

  getTextDecoration () {
    if (!(this.highlight && this.tickets)) {
        return '';
    }
    return this.tickets.reduce(function (b, annotation) {
      if ((annotation.status === 'pending'
           || annotation.status === 'accepted')
          && annotation.body.subtype === 'delete'
          && annotation.creator !== 'earlyPrint'
          && b === '') {
        return 'line-through';
      }
      return b;
    }, '')
  }

  /**
   * Toggles a CSS class on or off.
   * Borrowed from LegacyElementMixin because importing that doesn't work and
   * this method is all we need from it.
   *
   * @param {string} name CSS class name
   * @param {boolean=} bool Boolean to force the class on or off.
   *    When unspecified, the state of the class will be reversed.
   * @param {Element=} node Node to target.  Defaults to `this`.
   * @return {void}
   * @override
   */
  toggleClass(name, bool, node) {
    node = /** @type {Element} */ (node || this);
    if (arguments.length == 1) {
      bool = !node.classList.contains(name);
    }
    if (bool) {
      node.classList.add(name);
    } else {
      node.classList.remove(name);
    }
  }

}

customElements.define('atomic-node', AtomicNode);

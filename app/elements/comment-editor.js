import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

import { Annotation } from './annotation.js';

class CommentEditor extends PolymerElement {
  static get template() {
    return html`
    <style>
    :host {
      display:block;
    }
    .editor-row.no-title {
      margin-top: 0.75rem;
    }
    .actions {
      margin-top: 0.75rem;
      -webkit-display: -webkit-box;
      -webkit-display: -webkit-flex;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      align-items: center;
      justify-content: flex-end;
    }

    .btn {
      border: none;
      border-radius: 2px;
      margin: 5px 1px;
      text-transform: uppercase;
      letter-spacing: 0;
      will-change: box-shadow, transform;
      transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1), color 0.2s cubic-bezier(0.4, 0, 0.2, 1);
      outline: 0;
      cursor: pointer;
      text-decoration: none;
      background: 0 0;
    }

    .btn:hover {
      color: #333;
      background-color: #e6e6e6;
      border-color: #adadad;
    }

    .btn:disabled {
      opacity: 0.5;
      cursor: not-allowed;
    }

    .btn:disabled:hover {
      background-color: transparent;
      color: inherit;
    }

    textarea {
      background: #ddd;
      display: inline-block;
      position: relative;
      width: 100%;
      border: 1px solid;
      padding: 2px;
      overflow: hidden;
      resize: none;
      font-family: inherit;
    }
    input[type="checkbox"] {
      width: 18px;
      height: 18px;
      vertical-align: middle;
      margin-right: 0.5rem;
    }
    </style>

    <div class="editor-row no-title">
      <textarea id="commentInput" rows="15" value="{{comment.value}}" label="Comment" name="comment" autofocus="true"></textarea>
    </div>
    <div class="editor-row no-title">
      <input type="checkbox" checked="{{private.value}}" id="privateInput" value="private" name="private"></input>
      <label for="privateInput">this is a private comment</label>
    </div>

    <div class="actions">
      <button class="btn cancel_button" on-click="cancel">Cancel</button>
      <button on-click="save" class="btn cancel_button" disabled="[[submitDisabled]]">save</button>
    </div>
`;
  }

  static get properties() {
    return {
      comment: {
        type: Object,
        value: ''
      },
      private: Object,
      submitDisabled: {
        type: Boolean,
        value: true
      }
    }
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
    const commentInput = this.shadowRoot.getElementById("commentInput");
    commentInput.addEventListener('input', event => this._handleValueChange(event));
    const privateInput = this.shadowRoot.getElementById("privateInput");
    privateInput.addEventListener('change', event => this._handleValueChange(event));
    this._reset();
    this._checkSubmitDisabled();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    const commentInput = this.shadowRoot.getElementById("commentInput");
    commentInput.removeEventListener('input', event => this._handleValueChange(event));
    const privateInput = this.shadowRoot.getElementById("privateInput");
    privateInput.removeEventListener('change', event => this._handleValueChange(event));
  }

  setTargets(targets) {
    function realTarget(element) {
      if (element.orig) { return element.orig }
      return element
    }
    this.targets = targets.map(realTarget)
  }

  save() {
    this.dispatchEvent(new CustomEvent('saved', {
      bubbles: true,
      composed: true,
      detail: {
        ticket: Annotation.getNewSessionKey(),
        id: this.comment.annotationId,
        body: this.comment.value,
        targets: this.targets,
        visibility: this.private.value ? 'private' : 'public',
        action: Annotation.ActionTypes.COMMENT
      }
    }));
    this._reset();
    this._checkSubmitDisabled();
    this.dispatchEvent(new CustomEvent('closed', { bubbles: true, composed: true }));
  }

  cancel() {
    this.dispatchEvent(new CustomEvent('cancelled', { bubbles: true, composed: true }));
    this._reset();
    this.dispatchEvent(new CustomEvent('closed', { bubbles: true, composed: true }));
  }

  _handleValueChange(event) {
    this[event.target.name].dirty = true;
    this[event.target.name].value = event.target.name === 'private'
                                    ? event.target.checked
                                    : event.target.value;
    this._checkSubmitDisabled();
  }

  _reset() {
    this.comment = {
      value: '',
      dirty: false
    }
    this.private = {
      value: false,
      dirty: false
    }
  }

  _checkSubmitDisabled() {
    this.submitDisabled =
      !((this.comment.value.length > 0) && (this.private.dirty || this.comment.dirty));
  }
}

customElements.define('comment-editor', CommentEditor);

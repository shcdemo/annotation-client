import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

class SelectionInfo extends PolymerElement {
  static get template() {
    return html`
    <style>
      .title {
        padding: 1rem 2rem;
        background: #eee;
        border-bottom: 1px solid grey;
      }
      .button-0:hover {
        background-color: #cec9c9;
        cursor: pointer;
      }
      .button-0 {
        float: right;
        padding: 0 0 0 0;
        min-width: 1.0em;
        margin: 0 0 0 0;
        background-color: transparent;
        border: none;
      }
    </style>
    <header class="title">
        <em>[[selectionInfo]]</em>
        <button class="button-0" aria-hidden="true" on-click="closePanel">X</button>
    </header>
`;
  }

  static get properties() {
    return {
      selectionInfo: {
        type: String,
        value: 'Log in to access annotations.'
      }
    }
  }

  getSelectionInfo(selection) {
    const user = sessionStorage.getItem("user") || 'guest';
    if (!user || user === 'guest') { return 'Log in to access annotations.' }
    if (!selection) { return 'Nothing selected' }
    var numberOfWords = selection.reduce(function (result, next) {
      if (next.nodeName !== 'TEI-C') {
        return result + 1
      }
      return result
    }, 0);
    if (numberOfWords > 1) {
      return numberOfWords + ' words selected';
    }
    var singleWord = selection[0];
    return 'word ' + singleWord.id + ' selected';
  }

  setSelection(targets) {
    this.selectionInfo = this.getSelectionInfo(targets);
  }

  closePanel() {
    window.dispatchEvent(new CustomEvent('closeAnnotationPanel', { bubbles: true, composed: true }));
  }
}

customElements.define('selection-info', SelectionInfo);
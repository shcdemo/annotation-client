'use strict';
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

class AnnotationBase extends PolymerElement {
  constructor() {
    super();

    this.ActionTypes = {
      UPDATE: 'update',
      JOIN: 'join',
      SPLIT: 'split',
      DELETE: 'delete',
      NOTE: 'note',
      COMMENT: 'comment',
      UPDATE_ATTRIBUTE: 'update.attribute'
    };

    this.ActionTypes.ALL = [
      this.ActionTypes.UPDATE,
      this.ActionTypes.JOIN,
      this.ActionTypes.SPLIT,
      this.ActionTypes.DELETE,
      this.ActionTypes.NOTE,
      this.ActionTypes.COMMENT,
      this.ActionTypes.UPDATE_ATTRIBUTE
    ];

    this.Corrections = ['update', 'join', 'split', 'delete'];

    this.getNewSessionKey = function () {
      var now = new Date();
      return 's-' + now.getTime();
    };

    this.createTempId = function () {
      var now = new Date();
      return 'tmp-' + now.getTime();
    };

    this.itemEditable = {
      openEditor: function () {
      }
    };

    this.itemSelectable = {
      properties: {
        selectable: {
          value: true
        }
      }
    };

    this.textReducer = function textReducer(text, element) {
      var etext;
      // always annotate original content
      if (!element.atomic && element.orig) {
        etext = element.orig.original;
      }
      else if (element.atomic && element.original) {
        etext = element.original;
      }
      else if (element.atomic && element.firstChild) {
        etext = element.firstChild.textContent;
      }
      else {
        // desperate last attempt
        etext = element.innerHTML.trim();
      }
      // Eliminate inner tagging such as <mark>
      var elem = new DOMParser().parseFromString(etext, 'text/html');
      etext = elem.body.textContent || "";
      return text += etext;
    };

    this.Selection = {};
    this.Selection.getElementsInRange = function elementsInRange(range) {
      var start = range[0];
      var end = range[1];
      var elements = [start];

      // a single element selected
      if (!end || start.id === end.id) {
        return elements;
      }

      var endId = end.id;
      function isEndElement(element) {
        if (element.orig) { return endId === element.orig.id }
        return endId === element.id;
      }

      // common case
      // find all atomic sibling elements between start and end
      // stop on first non-atomic sibling (choice), end of siblings and
      // end element reached
      var sibling = start;
      do {
        sibling = sibling.nextElementSibling
        // skip eventual tooltip div
        if (!sibling || sibling.nodeName === 'DIV') {
          continue
        }
        if (sibling.classList.contains("atomic") || sibling.classList.contains("selectable")) {
          console.debug('add', sibling, 'to selection');
          elements.push(sibling);
        }
      } while (sibling && !isEndElement(sibling));

      return elements;
    }

//    this.atomic = AtomicNode;
  } // end monster constructor

}

customElements.define('annotation-base', AnnotationBase);
export const Annotation = new AnnotationBase();

import { AtomicNode } from './atomic-node.js';
import { Annotation } from './annotation.js';

class TeiPc extends AtomicNode {
  static get is() {
    return 'tei-pc';
  }
  static get behaviors() {
    return [
      Annotation.atomic.actions,
      Annotation.itemEditable,
      Annotation.itemSelectable
    ];
  }
}

customElements.define(TeiPc.is, TeiPc);
/*
  NOTE: This file is for apps that have
  annotation-client as a bower dependency
*/
import { Annotation } from './annotation.js';
import './annotation-panel.js';

import './annotatable-document.js';
import './tei-w.js';
import './tei-c.js';
import './tei-pc.js';

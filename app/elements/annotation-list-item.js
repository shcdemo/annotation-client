import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

import { Annotation } from './annotation.js';

class AnnotationListItem extends PolymerElement {
  static get template() {
    return html`
    <style>
  /* TODO visualize visibility */
  .annotation-details:after {
    visibility: hidden;
    display: block;
    font-size: 0;
    content: " ";
    clear: both;
    height: 0;
  }
  .annotation-type {
    padding:1rem 0;
    background-color: black;
    color: white;
  }
  .part {
    float: left;
    width: 50%;
  }
  .annotation-detail_label {
    display: inline-block;
    color: #888;
  }
  </style>
    <div class="annotation-details">
      <template is="dom-if" if="[[showOldValue(item.body.subtype, item.body.data)]]">
        <div class="part">
          <div><span class="annotation-detail_label">[[getAnnotationBadge(item.body.subtype, item.body.data)]]</span></div>
          <div><span class="original-text">[[item.body.originalValue]]</span></div>
        </div>
        <div class="part">
          <div><span class="annotation-detail_label">[[getTo(item.body.subtype)]]</span></div>
          <div><span class="annotation-body">[[item.body.display]]</span></div>
        </div>
      </template>
      <template is="dom-if" if="[[isNote(item.body.subtype)]]">
        <div><span class="annotation-detail_label">[[getAnnotationBadge(item.body.subtype, item.body.data)]]</span></div>
        <div>[[item.body.display]]</div>
      </template>
      <template is="dom-if" if="[[isMarkAsDefect(item.body.subtype, item.body.data)]]">
        <div><span class="annotation-detail_label">[[getDefectState(item.body.display)]]</span></div>
      </template>
    </div>
`;
  }

  static get properties() {
    return {
      item: {
        type: Object
      }
    }
  }

  getAnnotationBadge(subtype, data) {
    var type;
    switch (subtype) {
      case Annotation.ActionTypes.UPDATE_ATTRIBUTE:
        type = 'change ' + (data ? data.attributeName : '');
        break;
      default:
        type = subtype;
    }
    return type;
  }

  getPreposition(subtype) {
    var type;
    switch (subtype) {
      case Annotation.ActionTypes.SPLIT:
      case Annotation.ActionTypes.JOIN:
        type = '';
        break;
      default:
        type = 'from';
    }
    return type;
  }

  getTo(subtype) {
    var type;
    switch (subtype) {
      case Annotation.ActionTypes.COMMENT:
        type = '';
        break;
      case Annotation.ActionTypes.SPLIT:
        type = 'into';
        break;
      default:
        type = 'to';
    }
    return type;
  }

  getDefectState(display) {
    if (display === 'true') {
      return 'marked as defective';
    }
    return '';
  }

  isNote(subtype) {
    return (subtype === Annotation.ActionTypes.COMMENT ||
      subtype === Annotation.ActionTypes.NOTE);
  }

  isMarkAsDefect(subtype, data) {
    return (
      subtype === Annotation.ActionTypes.UPDATE_ATTRIBUTE &&
      data &&
      data.attributeName === 'defect'
    );
  }

  showOldValue(subtype, data) {
    return (!this.isNote(subtype) && !this.isMarkAsDefect(subtype, data));
  }

  _changeHandler(item) {
    console.log('_changeHandler', item);
  }
}

customElements.define('annotation-list-item', AnnotationListItem);

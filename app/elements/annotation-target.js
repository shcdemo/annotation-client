import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { FlattenedNodesObserver as PolymerFlattenedNodesObserver } from '@polymer/polymer/lib/utils/flattened-nodes-observer.js';

import { Annotation } from './annotation.js';

export class AnnotationTarget extends PolymerElement {

  /*
  
  Example:
  <annotation-target source="document1">
    <target-selector type="IdSelector" value="foo" />
  </annotation-target>
  
  CSSSelector Example:
  <annotation-target source="document1">
    <target-selector type="CSSSelector" value="#foo" />
  </annotation-target>
  
  TextRange Example:
  <annotation-target source="document1">
    <target-selector type="RangeSelector">
      <start-selector type="IdSelector" value="foo"></start-selector>
      <end-selector type="IdSelector" value="bar"></start-selector>
    </target-selector>
  </annotation-target>
  
  */

  static get properties() {
    return {
      source: {
        type: String,
        reflectToAttribute: true
      },
      version: {
        type: String,
        reflectToAttribute: true
      },
      format: {
        type: String,
        reflectToAttribute: true
      },
      limits: {
        type: Array,
        value: function () { return [] }
      },
      disabled: Boolean
    }
  }

  factoryImpl(data) {
    console.log('<annotation-target>.factory', data.targets, data.document)
    this.source = data.document.source;
    this.version = data.document.version
    // reflectToAttribute is not working, so do this explicitly
    this.setAttribute("source", data.document.source);
    this.setAttribute("version", data.document.version);

    var doc = document.getElementById(data.document.id)
    if (!doc) {
      throw Error(
        '<annotation-target>.factory: no document found',
        this.source
      )
    }

    var start, end, selectorElement

    start = data.targets[0]

    if (data.targets.length === 1) {
      selectorElement = this.buildIdSelectorElement('target-selector', start)
      // enforce closing tag
      selectorElement.appendChild(document.createTextNode(''))
      this.limits = [selectorElement]
    }

    if (data.targets.length > 1) {
      end = data.targets[data.targets.length-1]
      var startSelector = this.buildIdSelectorElement('start-selector', start)
      var endSelector = this.buildIdSelectorElement('end-selector', end)

      selectorElement = document.createElement('target-selector')
      selectorElement.setAttribute('type', 'RangeSelector') // set type for range
      selectorElement.appendChild(startSelector)
      selectorElement.appendChild(endSelector)
      this.limits = [startSelector, endSelector]
    }

    this.appendChild(selectorElement)
  }

  constructor(data) {
    super();
    if (data) {
      this.factoryImpl(data);
    }
  }

  ready() {
    Annotation.Target = AnnotationTarget;
  }

  connectedCallback() {
    super.connectedCallback();
    if (this.limits.length === 0) {
      this.limits = this._getSelector(this);
    }
    this.dispatchEvent(new CustomEvent('annotation-target.attached', { bubbles: true, composed: true }));
  }

  // The targets could be just about anywhere in the light or shadow DOM, so we
  // may have to dig deep to find them.
  getTargets() {
    return this.limits.flatMap(selector => {
      // workaround for malformed CSSSelector values
      const type = selector.getAttribute('type');
      const value = selector.getAttribute('value');
      // Try content-inner, then fall back to body.
      let root = document.getElementById('content-inner');
      if (!root) {
        root = document.querySelector('body');
      }
      let targets = null;
      if (type === 'IdSelector') {
        targets = this._querySelectorAllDeep(root, `#${value}`);
      }
      else if (type === 'CSSSelector') {
        targets = this._querySelectorAllDeep(root, value);
      }
      if (!targets) {
        console.log(`There's no there there for ${value}.`);
      }
      return targets;
    });
  }

  _querySelectorDeep(node, selector) {
    const nodeIterator = document.createNodeIterator(node, Node.ELEMENT_NODE);
    let foundNode = node.querySelector(selector);
    let currentNode;
    while (!foundNode && (currentNode = nodeIterator.nextNode())) {
      if(currentNode.shadowRoot) {
        foundNode = this._querySelectorDeep(currentNode.shadowRoot, selector);
      }
    }
    return foundNode;
  }

  _querySelectorAllDeep(node, selector) {
    const nodes = [...node.querySelectorAll(selector)],
          nodeIterator = document.createNodeIterator(node, Node.ELEMENT_NODE);
    let currentNode;
    while (currentNode = nodeIterator.nextNode()) {
      if(currentNode.shadowRoot) {
        nodes.push(...this._querySelectorAllDeep(currentNode.shadowRoot, selector));
      }
    }
    return nodes;
  }

  _myQueryEffectiveChildren(selector) {
    const x = PolymerFlattenedNodesObserver
                .getFlattenedNodes(this)
                .filter(n => n.nodeType === Node.ELEMENT_NODE)
                .filter(n => n.parentNode.querySelector(selector) === n);
    return x.length > 0 && x[0] || [];
  }

  _getSelector(targetElement) {
    var selectorElement = targetElement._myQueryEffectiveChildren('target-selector');
    var selectorType = selectorElement.getAttribute('type');
    if (selectorType === 'RangeSelector') {
      var start = selectorElement.querySelector('start-selector');
      var end = selectorElement.querySelector('end-selector');
      if (!start || !end) {
        console.error('incomplete or incompatible RangeSelector', selectorElement);
      }
      return [start, end];
    }
    if (selectorType === 'IdSelector' || selectorType === 'CSSSelector') {
      return [selectorElement];
    }
    console.warn('Unsupported selector type', selectorElement.getAttribute('type'));
  }

  buildIdSelectorElement(nodeName, data) {
    if (!data) { return null }
    var node = document.createElement(nodeName);
    node.setAttribute('type', 'IdSelector');
    node.setAttribute('value', data.id);
    return node;
  }
}

customElements.define('annotation-target', AnnotationTarget);
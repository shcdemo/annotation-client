# high level overview of annotation architecture

This text shortly sketches the architecture of the client-side webcomponents involved with the annotations.

Annotations will be represented as Web Components themselves and live as a list
of annotations somewhere in the body of the document.

Example:

```<annotation-list>
    <annotation-item...>(...)</annotation-item>
    <annotation-item...>(...)</annotation-item>
    <annotation-item...>(...)</annotation-item>
    ...
</annotation-list>

(...)

<tei-w>
    ...text...
    <shadow-root1>revised text</shadow-root1>
</tei-w>
```
The general idea is a MVC model where the annotation-list represents the model.

An annotation targets a certain element (the tei-w here) which can be intialized at page load.
The relevant part of the annotation (still to be worked out for the different cases) will be
attached to the annotated elements shadow DOM. This is conceptually a very desireable model as
the annotations really stay 'standoff' - the shadow DOM nodes are technically a document fragment
that is not part of the main DOM but can be rendered as part of the document DOM.

While there's this nice conceptual fit handling shadow DOM comes to a price:
* CSS styling might not be easy due to style encapsulation, special rules to be
learned and applied etc.
* DOM navigation requires using a new API which must be mastered.

The same results surely can also be achieved without shadow DOM just by applying
appropriate CSS matchers allowing a certain annotation to be shown/hidden.

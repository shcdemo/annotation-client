# Early Modern Lab - Annotation Client

Start up demo/dev server with

```bash
    npm install
    bower install
    npm start
```

A web components application to annotate texts derived from TEI documents. (but not limited to TEI)
- extensible, open library

Dependent on
- annotation-service

Current assumptions
- TEI elements will be mapped to web-components (tei: -> tei-)
- TEI-elements will not be directly edited
- Annotations will use the [Web Annotation Data Model]() to connect to the
  target element

----
Users annotate by 1. clicking on any element, 2. writing something and 3. save
annotation
----

Uses
- riotjs or polymer (starterkit)
- gulp
- annotator.js
- bower or npm

Later
- annotate semantic changes
- annotations and comments on annotations
